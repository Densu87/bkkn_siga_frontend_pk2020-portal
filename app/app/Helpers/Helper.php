<?php
function debug($obj, $desc=1)
{
    echo '<pre>';
    if ($desc) {
        print_r($obj);
    } else {
        var_dump($obj);
    }
    echo '</pre>';
}

function getExceptionMessage($e, $http_code=500) 
{   
    http_response_code($http_code);
    $dev = 1;
    if ($dev==1) {
        return $e->getMessage();
    } else {
        return 'Error has occured.';
    }
}

function currentUser($obj='ID')
{
    /* if (auth()->check()) {
        $user = auth()->user(); 
        return $user[$obj];
    } */
    $result = session('user.'.$obj);
    return $result;
}

function getMenu()
{
    if (empty(session('usermenu'))) {    
        $menumodel = new \App\Models\Menu();
        $menu = $menumodel->getTopNav(currentUser('RoleID'));
    }
    $sessmenu = request()->session()->get('usermenu');
    return $sessmenu;
    
}


function logError($activity, $data, $message){
    try {
        $log = DB::table('log_error')->insert(
            [
                'activity' => $activity,
                'data' => $data,
                'message' => $message,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => currentUser('UserName'),
            ]
        );
    } catch (\Exception $ex) {
        $log = 0;
        // debug($ex->getMessage());
    }
    return $log;
}
