<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\DataTable;
use App\Libraries\jqGrid;

use App\Models\Sensus;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $msensus = new Sensus();

        $count = Sensus::count();
        //$datasensus = $msensus->getAll();
        //return view('home', compact('datasensus'));
        $items = $request->items ?? 10;
        $club = $msensus->getDataTable();
        $valid = $msensus->getDataTableNotValid();
        $members = $club->paginate($items);
        
        //return view('home');
        return view('home', ['members' => $members, 'items' => $items, 'total' => $count, 'notv' => $valid]);
    }
    
    public function home()
    {
        return view('home');
    }
    
    
    public function statussensus()
    {
        $sql = 'SELECT  COALESCE(y."Value", \'Lainnya\') as nama_status, sum(cnt) as jumlah
FROM (
SELECT CASE WHEN status_sensus IN (\'1\',\'2\',\'3\',\'4\') THEN status_sensus
ELSE \'0\'
END AS status, count(*) cnt 
FROM mst_formulir a
GROUP BY status_sensus
) x 
LEFT JOIN "Parameter" y ON y."Code"=x.status AND y."Group"=\'StatusSensus\'
GROUP BY x.status, y."Value"
order by status';
        $rows = \DB::select($sql);
        $labels = [];
        $data = [];
        $backgroundColor = ['#b2d4f5', '#fcc3a7', '#8fd1bb', '#f8b4c9', '#d3bdeb', '#83d1da', '#99a0f9', '#e597d2', '#d1d9dc', '#fccaca', '#85a1bb'];
        $totaldata = 0;
        foreach ($rows as $row) {
            $labels[] = $row->nama_status;
            $data[] = $row->jumlah;
            $totaldata += $row->jumlah;
            // $backgroundColor[] = $row->jumlah;
        };
        $chartdata = [
            'labels' => $labels,
            'datasets' => [
                [
                    'backgroundColor' => $backgroundColor,
                    'data' => $data
                ]
            ],
            'customdata' => ['totaldata'=>$totaldata]
        ];
        return $this->jsonOutput($chartdata);
    }
    
    public function datasensus()
    {
        
        // $msensus = new Sensus();
        // $rows = $msensus->where('periode_sensus','2020')->orderBy('create_date', 'DESC')->take(10)->get();
        $sql = '
SELECT a.*, h.nama_provinsi 
FROM mst_formulir a
LEFT JOIN "RT" b  ON b.id_rt=a.id_rt
LEFT  JOIN "RW" d ON d.id_rw=a.id_rw
LEFT  JOIN "Kelurahan" e ON e.id_kelurahan=a.id_desa
LEFT  JOIN "Kecamatan" f ON f.id_kecamatan=a.id_kecamatan
LEFT  JOIN "Kabupaten" g ON g.id_kabupaten=a.id_kabupaten
LEFT  JOIN "Provinsi" h ON h.id_provinsi=a.id_propinsi
ORDER BY create_date DESC 
LIMIT 10';
$rows = \DB::select($sql);
        $result = [];
        foreach ($rows as $row) {
            $item = [];
            $item['alamat'] = $row->alamat1;
            $item['wilayah'] = $row->nama_provinsi;
            $item['jml_jiwa'] = $row->jml_jiwa;
            $item['create_date'] = $row->create_date;
            $item['status_sensus'] = $row->status_sensus;
            $result[] = $item;
        }
        return $this->jsonOutput($result);
        
    }
    
    public function dailysumdata()
    {
        $sql = '
SELECT * FROM (
select create_date, count(*) cnt FROM mst_formulir
WHERE create_date is not null
GROUP BY create_date
ORDER BY create_date desc
LIMIT 5
) x ORDER BY create_date';
        $rows = \DB::select($sql);
        $labels = [];
        $data = [];
        foreach ($rows as $row) {
            $labels[] = date_format(date_create($row->create_date), 'd/m');
            $data[] = $row->cnt;
        };
        $chartdata = [
            'labels' => $labels,
            'datasets' => [
                [
                    'backgroundColor' => '#5d78ff',
                    'data' => $data
                ]
            ]
        ];
        return $this->jsonOutput($chartdata);
    }
    

    public function statPendata()
    {
        if (auth()->check()) {
            $rows = \App\Models\User::select('TingkatWilayahID', \DB::raw('count(*) as total'))
                    ->where('RoleID',5)
                    ->where('CreatedBy', currentUser('UserName'))
                    ->groupBy('TingkatWilayahID')
                    ->get();
        } else {
            $rows = \App\Models\User::select('TingkatWilayahID', \DB::raw('count(*) as total'))
                    ->where('RoleID',5)
                    ->groupBy('TingkatWilayahID')
                    ->get();
        }
        $result['total'] = $rows->sum('total');
        $result['data'] = $rows->toArray();
        return $this->jsonOutput($result);
    }
    


    public function dataPaging()
    {
        $sensusmodel = new Sensus();
        $sensus = $sensusmodel->getAll();
        return $this->jsonOutput($sensus);
    }

    public function failData()
    {
        return [7, 12, 30, 16, 12, 11, 13, 9, 13, 15, 9];
    }

    public function successData()
    {
        return [14, 12, 14, 15, 19, 11, 10, 12, 13, 11, 14];
    }
}
