<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wilayah;
use App\Models\UserAkses;

use App\Models\User;

use App\Libraries\DataTable;
use App\Libraries\jqGrid;

use DB;

class WilayahController extends Controller
{
    public function akses()
    {
        $model = new Wilayah();
        $rows = $model->getTreeWilayah2();
        return $this->jsonOutput($rows);
    }

    public function updateAkses()
    {
        $model = new UserAkses();
        $rows = $model->postUpdateAkses();
        return $this->jsonOutput($rows);
    }


    public function provinsi($id)
    {
        $wilmodel = new Wilayah();
        $rows = $wilmodel->getProvinces($id);
        return $this->jsonOutput($rows);
    }

    public function kotakab($id)
    {
        $wilmodel = new Wilayah();
        $rows = $wilmodel->getRegencies($id);
        return $this->jsonOutput($rows);
    }

    public function kecamatan($id)
    {
        $wilmodel = new Wilayah();
        $rows = $wilmodel->getDistricts($id);
        return $this->jsonOutput($rows);
    }

    public function kelurahan($id)
    {
        $wilmodel = new Wilayah();
        $rows = $wilmodel->getVillages($id);
        return $this->jsonOutput($rows);
    }

    public function rw($id)
    {
        $wilmodel = new \App\Models\Master\RW();
        $rows = $wilmodel->getByParent($id);
        return $this->jsonOutput($rows);
    }

    public function rt($id)
    {
        $wilmodel = new \App\Models\Master\RT();
        $rows = $wilmodel->getByParent($id);
        return $this->jsonOutput($rows);
    }

    public function wilayahkoe()
    {
        //$wilmodel = new Wilayah();
        $rows = Kelurahan::all(); // $wilmodel->getWialayah();
        return $this->jsonOutput($rows);
    }

    public function index()
    {

        $userID = currentUser('ID');
        $userAkses = UserAkses::where('UserID', $userID)->get()->pluck('WilayahID')->toArray();
        $valwilayah = 1;
        $model = new Wilayah;
        $provinsi = $model->getDataProvinsi();       
        //$kabupaten = $model->getDataKabupaten(id);       

        //debug($wilayah); exit;

        return view('wilayah.wilayahindex')->with(compact('provinsi', 'valwilayah'));
    }

    public function getDataProvinsi()
    {
        $sql = 'select * from "Provinsi" order by id_provinsi';

        $rows = \DB::select($sql);

        $collection = $rows; //$this->with(['role','wilayah'])->orderby('ID', 'desc');
        //$data = new DataTable($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        //$data = new jqGrid($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        $data = new jqGrid($sql, ['searchFields'=>['nama_provinsi', 'id_provinsi']]);
        $users = $data->get();

        return $this->jsonOutput($users);
    }


    public function getDataKabupaten($id)
    {
        $sql = 'select * from "Kabupaten" where id_provinsi = '.$id.' order by id_kabupaten asc';

        $rows = \DB::select($sql);

        $collection = $rows; //$this->with(['role','wilayah'])->orderby('ID', 'desc');
        //$data = new DataTable($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        //$data = new jqGrid($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        $data = new jqGrid($sql, ['searchFields'=>['nama_kabupaten', 'id_kabupaten']]);
        $kabupaten = $data->get();

        return $this->jsonOutput($kabupaten);
    }

    public function getDataKecamatan($id)
    {
        $sql = 'select * from "Kecamatan" where id_kabupaten = '.$id.' order by id_kecamatan asc ';

        $rows = \DB::select($sql);

        $collection = $rows; //$this->with(['role','wilayah'])->orderby('ID', 'desc');
        //$data = new DataTable($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        //$data = new jqGrid($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        $data = new jqGrid($sql, ['searchFields'=>['nama_kecamatan', 'id_kecamatan']]);
        $kecamatan = $data->get();

        return $this->jsonOutput($kecamatan);
    }

    public function getDataKelurahan($id)
    {
        $sql = 'select * from "Kelurahan" where id_kecamatan = '.$id.' order by id_kelurahan asc';

        $rows = \DB::select($sql);

        $collection = $rows; //$this->with(['role','wilayah'])->orderby('ID', 'desc');
        //$data = new DataTable($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        //$data = new jqGrid($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        $data = new jqGrid($sql, ['searchFields'=>['nama_kelurahan', 'id_kelurahan']]);
        $kecamatan = $data->get();

        return $this->jsonOutput($kecamatan);
    }


    public function getDataRw($id)
    {
        $sql = 'select * from "RW" where id_kelurahan = '.$id.' order by id_rw desc';

        $rows = \DB::select($sql);

        $collection = $rows; //$this->with(['role','wilayah'])->orderby('ID', 'desc');
        //$data = new DataTable($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        //$data = new jqGrid($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        $data = new jqGrid($sql, ['searchFields'=>['nama_rw', 'id_rw']]);
        $kecamatan = $data->get();

        return $this->jsonOutput($kecamatan);
    }

    public function getDataRt($id)
    {
        $sql = 'select * from "RT" where id_rw = '.$id.' order by id_rt desc';

        $rows = \DB::select($sql);

        $collection = $rows; //$this->with(['role','wilayah'])->orderby('ID', 'desc');
        //$data = new DataTable($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        //$data = new jqGrid($collection, ['searchFields'=>['UserName', 'NamaLengkap']]);
        $data = new jqGrid($sql, ['searchFields'=>['nama_rt', 'id_rt']]);
        $kecamatan = $data->get();

        return $this->jsonOutput($kecamatan);
    }


    public function AddPostRW(Request $request)
    {
        $usermodel = new Wilayah();
        $result = $usermodel->postAddRW();
        return $this->jsonOutput($result);
    }

    public function EditPostRW(Request $request)
    {
        $edit = new Wilayah();
        $result = $edit->postEditRW();
        return $this->jsonOutput($result);
    }

    public function AddPostRT(Request $request)
    {
        $usermodel = new Wilayah();
        $result = $usermodel->postAddRT();

        return $this->jsonOutput($result);
    }


    public function EditPostRT(Request $request)
    {
        $edit = new Wilayah();
        $result = $edit->postEditRT();
        return $this->jsonOutput($result);
    }

    // Ubah wilayah Parent (Provinsi, Kabupaten, Kecamatan, Kelurahan)
    public function UbahWilayahParent ($id, $Key, $kondisi) {
        
        
        $sqlUpdate = '';

        $sqlrt = 'update "RT" a
                   set "KelurahanID"      = b.id_kelurahan
                        , "KodeKelurahan" = b."KodeDepdagriKelurahan"
                        , "KecamatanID"   = b.id_kecamatan
                        , "KodeKecamatan" = b."KodeDepdagriKecamatan"
                        , "KabupatenID"   = b.id_kabupaten
                        , "KodeKabupaten" = b."KodeDepdagriKabupaten"
                        , "ProvinsiID"    = b.id_provinsi
                        , "KodeProvinsi"  = b."KodeDepdagriProvinsi"
                    from v_data_wilayah_rw b        
                    where a.id_rw = b.id_rw '; 

        if ($kondisi == 1) {             
             $sqlUpdate = 'Update "Kabupaten" set id_provinsi = \''.$id.'\' where id_kabupaten = \''.str_replace(' ', '', $Key).'\'';
             $sqlrt =  $sqlrt. ' and a.id_kabupaten =  \''.str_replace(' ', '', $Key).'\'';                          
        }  else if ($kondisi == 2) {                          
             $sqlUpdate = 'Update "Kecamatan" set id_kabupaten = \''.$id.'\' where id_kecamatan = \''.str_replace(' ', '', $Key).'\'';
             $sqlrt =  $sqlrt. ' and a.id_kecamatan = \''.str_replace(' ', '', $Key).'\'';                       
        }  else if ($kondisi == 3) {                             
             $sqlUpdate = 'Update "Kelurahan" set id_kecamatan = \''.$id.'\' where id_kelurahan = \''.str_replace(' ', '', $Key).'\'';
             $sqlrt =  $sqlrt. ' and a.id_kelurahan = \''.str_replace(' ', '', $Key).'\'';    
        }  else if ($kondisi == 4) {
             $sqlUpdate = 'Update "RW" set id_kelurahan = \''.$id.'\' where id_rw = \''.str_replace(' ', '', $Key).'\'';
             $sqlrt =  $sqlrt. ' and a.id_rw = \''.str_replace(' ', '', $Key).'\'';            
        } 

        try
        {
             DB::statement($sqlUpdate);

             // Update Data RT   
             DB::statement($sqlrt);  

             $result = ['status' => true,'message' => 'Yeaaah... its Success.']; 
        }
        catch(Exception $e)
        {
            $result = ['status' => false,'message' => $e->getMessage()];
        }
             

       

       return $result;
    }
}
