<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\TingkatWilayah;
use App\Helpers\Helper;

use Auth;
use Hash;
use Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkPermission:usermgmt')->only(['index','dataPaging','store','update','resetPassword','destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parameter = \DB::table('Parameter')->whereIn('Group',['Is1Kelurahan','MaxTarget','Pwd'])->pluck('Value','Code');
        return view('user.index')->with(compact('parameter'));
    }

    public function dataPaging()
    {
        $id = currentUser('RoleID');
        $usermodel = new User();
        $users = $usermodel->getDataPagingUserWil();
        return $this->jsonOutput($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usermodel = new User();
        $result = $usermodel->postCreateUser();
        return $this->jsonOutput($result);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usermodel = new User();
        $result = $usermodel->postUpdateUser($id);
        return $this->jsonOutput($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $usermodel = new User();
        $result = $usermodel->postDeleteUser($id);
        return $this->jsonOutput($result);
    }

    public function profile()
    {
        return view('user.profile');
    }
    
    public function showLoginForm()
    {
        if (auth()->check()) return redirect('/');
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $usermodel = new User();
        $result = $usermodel->login($request);
        return ($result->status) ? redirect('/') : redirect()->to('login')
                    ->withInput($request->input())
                    ->withErrors($result->message);
    }

    public function logout(Request $request)
    {
        \Auth::logout();
        $request->session()->invalidate();
        return redirect('/');
    }

    public function resetPassword(Request $request, $id)
    {
        $usermodel = new User();
        $result = $usermodel->postResetUser($id);
        return $this->jsonOutput($result);
    }

    public function changePassword()
    {
        $usermodel = new User();
        $result = $usermodel->postChangePassword();
        return $this->jsonOutput($result);
    }

    public function getRole()
    {
        $Roleid = currentUser('RoleID');
        $userRole = new Role();
        $result = $userRole->getRoleByID($Roleid);
        return $this->jsonOutput($result);
    }

    public function getTingkatWilayah($id)
    {
        $userWilayah = new TingkatWilayah();
        $result = $userWilayah->getDataListByRoleID($id);
        return $this->jsonOutput($result);
    }
    
    public function importUser()
    {   
        if (currentUser('RoleID')>=3) abort(403);
        return view('user.import');
    }
    
    public function processImport(Request $request)
    {
        
        \DB::table('User_Import')->where('CreatedBy',currentUser('UserName'))->delete();
        //$result = $request->file();
        $path = $request->file('file')->getRealPath();
        $file = file($path);
        $data = array_slice($file, 1);
        $rows = [];
        foreach ($data as $str) {
            $row = explode(';', $str);
            $item = [];
            $item['UserName'] = $row[0];
            $item['NamaLengkap'] = $row[1];
            $item['NIK'] = $row[2];
            $item['Alamat'] = $row[3];
            $item['NoTelepon'] = $row[4];
            $item['Email'] = $row[5];
            $item['NIP'] = $row[6];
            $item['WilayahID'] = $row[7];
            $item['CreatedBy'] = currentUser('UserName');
            $rows[] = $item;
            \DB::table('User_Import')->insert($item);
        }
        
        return $this->jsonOutput($rows);
    }
    
    public function processDataImport(Request $request)
    {   
        $rows = \DB::table('User_Import')->where('CreatedBy',currentUser('UserName'))->get();
        $data = [];
        $cnt = 0;
        foreach ($rows as $row) {
                $item = [];
                $item['ID'] = User::max('ID') + 1;
                $item['PeriodeSensus'] = \DB::table('PeriodeSensus')->where('IsOpen','Y')->first()->Tahun ?? '';
                $item['UserName'] = $row->UserName;
                $item['NamaLengkap'] = $row->NamaLengkap;
                $item['NIK'] = $row->NIK;
                $item['Alamat'] = $row->Alamat;
                $item['NoTelepon'] = $row->NoTelepon;
                $item['Email'] = $row->Email;
                $item['NIP'] = $row->NIP;
                $data[$cnt] = $item;
                $item['CreatedBy'] = currentUser('UserName');
                $item['Password'] = \Hash::make(\DB::table('Parameter')->where('Group','Pwd')->first()->Value ?? '');
                $item['RoleID'] = 3;
                $item['TingkatWilayahID'] = 3;
                $item['IsTemporary'] = 0;
                $item['IsActive'] = 1;
            try {
                User::create($item);
                \DB::table('User_Import')->where('CreatedBy',currentUser('UserName'))->where('UserName',$row->UserName)->delete();
                $data[$cnt]['Keterangan'] = 'OK';
            } catch (\Exception $ex) {
                $data[$cnt]['Keterangan'] = $ex->getMessage();
            }
            $cnt++;
        }
        return $this->jsonOutput($data);
    }
    
}
