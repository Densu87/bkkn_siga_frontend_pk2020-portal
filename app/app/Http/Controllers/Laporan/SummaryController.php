<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\jqGrid;
use PDF;

class SummaryController extends Controller
{
    public function index()
    {
        $valperiode = currentUser('PeriodeSensus');
        if (in_array(currentUser('RoleID'), [2,3])) {
            $valperiode = null;
        }
        $periode = \DB::table('PeriodeSensus')->where('IsOpen','Y')->orderBy('Tahun', 'DESC')->get();
        $tingkatwilayah = \DB::table('TingkatWilayah')->where('ID','>=',currentUser('TingkatWilayahID'))->orderBy('ID', 'ASC')->get();
        return view('laporan.summary_data')->with(compact('periode', 'valperiode', 'tingkatwilayah'));;
    }
    
    public function data()
    {
        
        $model = new \App\Models\Master\Kelurahan();
        $wilayah = $model->getByUserID();
        $whereKel = implode(',',$wilayah->pluck('id')->toArray());
        $periode = request()->input('PeriodeSensus');
        switch (request()->input('groupby')) {
            case 4:
                $sql = '
                WITH t1 AS (
                    SELECT DISTINCT id_desa, status_sensus, count(*) qty FROM mst_formulir WHERE id_desa IN ('.$whereKel.')
                    AND  COALESCE(status_sensus,\'\') <> \'\'
                    GROUP BY id_desa, status_sensus
                )
                , t2 AS (
                    SELECT "Code", "Value" FROM "Parameter" WHERE "Group"=\'StatusSensus\'
                )
                , w As (
                    SELECT DISTINCT "KelurahanID" as id_kelurahan, "NamaKelurahan" as nama_kelurahan FROM "RT" WHERE "KelurahanID" IN ('.$whereKel.')
                )
                
                SELECT DISTINCT w.id_kelurahan, w.nama_kelurahan, t2."Code" AS status_sensus, t2."Value" as status_nama, COALESCE(t3.qty, 0) qty 
                FROM t1 
                CROSS JOIN t2
                LEFT JOIN t1 AS t3 ON CAST(t3.status_sensus AS VARCHAR)=CAST(t2."Code" AS VARCHAR) AND t1.id_desa=t3.id_desa 
                LEFT JOIN  w ON w."id_kelurahan"=t1.id_desa
                ORDER BY w.id_kelurahan, t2."Code" ';
                                
                $sqlagg = 'SELECT id_kelurahan, nama_kelurahan, array_agg(status_sensus) as status_sensus, array_agg(status_nama) as status_nama, array_agg(quote_literal(status_sensus)||\':\'||qty) as qty 
                FROM ('.$sql.') x GROUP BY id_kelurahan, nama_kelurahan';
                break;
            case 5:
                $sql = '
                WITH t1 AS (
                    SELECT id_desa, id_rw, status_sensus, count(*) qty FROM mst_formulir WHERE id_desa IN ('.$whereKel.')
                    AND  COALESCE(status_sensus,\'\') <> \'\'
                    GROUP BY id_desa, id_rw, status_sensus
                )
                , t2 AS (
                    SELECT "Code", "Value" FROM "Parameter" WHERE "Group"=\'StatusSensus\'
                )
                , w As (
                    SELECT 
                    DISTINCT "KelurahanID" as id_kelurahan, "NamaKelurahan" as nama_kelurahan, id_rw, "NamaRW" as nama_rw FROM "RT" WHERE "KelurahanID" IN ('.$whereKel.')
                )
                
                SELECT DISTINCT w.id_kelurahan, w.nama_kelurahan, w.id_rw, w.nama_rw, t2."Code" AS status_sensus, t2."Value" as status_nama, COALESCE(t3.qty, 0) qty 
                FROM t1 
                CROSS JOIN t2
                LEFT JOIN t1 AS t3 ON CAST(t3.status_sensus AS VARCHAR)=CAST(t2."Code" AS VARCHAR) AND t1.id_desa=t3.id_desa AND t1.id_rw=t3.id_rw
                LEFT JOIN w ON w.id_rw=t1.id_rw
                ORDER BY w.id_kelurahan, w.id_rw, t2."Code" ';
                                
                $sqlagg = 'SELECT id_kelurahan, nama_kelurahan, id_rw, nama_rw, array_agg(status_sensus) as status_sensus, array_agg(status_nama) as status_nama, array_agg(quote_literal(status_sensus)||\':\'||qty) as qty 
                FROM ('.$sql.') x GROUP BY id_kelurahan, nama_kelurahan, id_rw, nama_rw';
                break;
            case 6:
                $sql = '
                WITH t1 AS (
                    SELECT id_desa, id_rw, id_rt, status_sensus, count(*) qty FROM mst_formulir WHERE id_desa IN ('.$whereKel.')
                    AND  COALESCE(status_sensus,\'\') <> \'\'
                    GROUP BY id_desa, id_rw, id_rt, status_sensus
                )
                , t2 AS (
                    SELECT "Code", "Value" FROM "Parameter" WHERE "Group"=\'StatusSensus\'
                )
                , w As (
                    SELECT 
                    DISTINCT "KelurahanID" as id_kelurahan, "NamaKelurahan" as nama_kelurahan, id_rw, "NamaRW" as nama_rw, id_rt, nama_rt FROM "RT" WHERE "KelurahanID" IN ('.$whereKel.')
                )
                
                SELECT DISTINCT w.id_kelurahan, w.nama_kelurahan, w.id_rw, w.nama_rw, w.id_rt, w.nama_rt, t2."Code" AS status_sensus, t2."Value" as status_nama, COALESCE(t3.qty, 0) qty 
                FROM t1 
                CROSS JOIN t2
                LEFT JOIN t1 AS t3 ON CAST(t3.status_sensus AS VARCHAR)=CAST(t2."Code" AS VARCHAR) AND t1.id_desa=t3.id_desa AND t1.id_rw=t3.id_rw AND t1.id_rt=t3.id_rt
                LEFT JOIN w ON w.id_rt=t1.id_rt
                ORDER BY w.id_kelurahan, w.id_rw, w.id_rt, t2."Code" ';
                                
                $sqlagg = 'SELECT id_kelurahan, nama_kelurahan, id_rw, nama_rw, id_rt, nama_rt, array_agg(status_sensus) as status_sensus, array_agg(status_nama) as status_nama, array_agg(quote_literal(status_sensus)||\':\'||qty) as qty 
                FROM ('.$sql.') x GROUP BY id_kelurahan, nama_kelurahan, id_rw, nama_rw, id_rt, nama_rt';
                break;
                
        }
        
        if (request()->input('print')==1) {
            $nama_wilayah = '';
            if (!empty(request()->input('RT'))) {
                $wilrow = \DB::table('v_rt')->where('id_rt', request()->input('RT'))->first();
                // $nama_wilayah = 'PROVINSI: ' . $wilrow->nama_provinsi . ', KOTA/KABUPATEN: ' . $wilrow->nama_kabupaten . ', KECAMATAN: ' . $wilrow->nama_kecamatan;
                $nama_wilayah .= 'DESA/KELURAHAN: ' . $wilrow->nama_kelurahan . ', RW: ' . $wilrow->nama_rw . ', RT: ' . $wilrow->nama_rt;
            } elseif (!empty(request()->input('RW'))) {
                $wilrow = \DB::table('v_rw')->where('id_rw', request()->input('RW'))->first();
                // $nama_wilayah = 'PROVINSI: ' . $wilrow->nama_provinsi . ', KOTA/KABUPATEN: ' . $wilrow->nama_kabupaten . ', KECAMATAN: ' . $wilrow->nama_kecamatan;
                $nama_wilayah .= 'DESA/KELURAHAN: ' . $wilrow->nama_kelurahan . ', RW: ' . $wilrow->nama_rw;
            } elseif (!empty(request()->input('Kelurahan'))) {
                $wilrow = \DB::table('v_kelurahan')->where('id_kelurahan', request()->input('Kelurahan'))->first();
                // $nama_wilayah = 'PROVINSI: ' . $wilrow->nama_provinsi . ', KOTA/KABUPATEN: ' . $wilrow->nama_kabupaten . ', KECAMATAN:' . $wilrow->nama_kecamatan;
                $nama_wilayah .= 'DESA/KELURAHAN: ' . $wilrow->nama_kelurahan;
            } else {
                $nama_wilayah = auth()->user()->wilayah->TingkatWilayah . ': ';
                $nama_wilayah .= implode(', ', auth()->user()->akseswilayah->pluck('nama_wilayah')->toArray());
            }
            $rows = \DB::select($sqlagg);
            // foreach ($rows as $row) {
                // $status = str_replace('\'', '"', $row->qty);
                // $status = json_decode($status, true);
            // }
            // return view('laporan.summary_pdf',['rows'=>$rows, 'periode'=>$periode, 'nama_wilayah'=>$nama_wilayah]);
            $pdf = PDF::loadview('laporan.summary_pdf',['rows'=>$rows, 'periode'=>$periode, 'nama_wilayah'=>$nama_wilayah]);
            return $pdf->stream();
        } else {
            $data = new jqGrid($sql);
            $result = $data->get();
            return $this->jsonOutput($result);
        }
    }
    
}
