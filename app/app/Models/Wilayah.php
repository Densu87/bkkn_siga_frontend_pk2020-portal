<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Wilayah extends BaseModel
{
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }

    public function getTreeWilayah()
    {
        $tabelWilayah = [
            1 => ['table'=>'Provinsi', 'parent'=>'RegionalID'],
            2 => ['table'=>'Kabupaten', 'parent'=>'id_provinsi'],
            3 => ['table'=>'Kecamatan', 'parent'=>'id_kabupaten'],
            4 => ['table'=>'Kelurahan', 'parent'=>'id_kecamatan'],
            5 => ['table'=>'RW', 'parent'=>'id_kelurahan'],
            6 => ['table'=>'RT', 'parent'=>'id_rw'],
        ];

        $result = [];
        $parentid= request()->input('nodeid') ? explode('_', request()->input('nodeid'))[1] : 0;
        $level= request()->input('n_level') ? request()->input('n_level')+1 : 1;
        $pTingkatUser = request()->input('tingkatUser');
        $userID = request()->input('userID');

        if ($parentid==0) {
            $userID = currentUser('ID');
            $user = User::find($userID);
            $wilayahID = UserAkses::where('UserID', $user->ID)->get()->pluck('WilayahID');
            $level = $user->TingkatWilayahID;
            $modelname = 'App\\Models\\Master\\'.$tabelWilayah[$level]['table'];
            $model = new $modelname;
            $rows = $model->getByID($wilayahID->toArray());
        } else {
            $modelname = 'App\\Models\\Master\\'.$tabelWilayah[$level]['table'];
            $model = new $modelname;
            $rows = $model->getByParent($parentid);
        }
        $userAkses = [];
        if ($level==$pTingkatUser) {
            $modelAkses = new UserAkses();
            $userAkses = $modelAkses->where('UserID', $userID)->get()->toArray();
        }
        foreach ($rows->toArray() as $row) {
            $item = [];
            $item = (array)$row;
            $item['id'] = $level.'_'.$row['id'];
            $item['level'] = $level;
            $item['parent'] = ($level-1).'_'.$parentid;
            $item['isLeaf'] = $level >= $pTingkatUser ? true : false;
            $item['WilayahID'] = $row['id'];
            $item['Flag'] = 0;
            $item['TargetKK'] = null;
            if ($level==$pTingkatUser) {
                foreach ($userAkses as $akses) {
                    if ($akses['WilayahID']==$row['id']) {
                        $item['Flag'] = 1;
                        $item['TargetKK'] = $akses['TargetKK'];
                    }
                }
            }
            $result[] = $item;
        }
        return $result;
    }

    public function getTreeWilayah2()
    {
        $result = [];
        $parentid= request()->input('nodeid') ? explode('_', request()->input('nodeid'))[1] : 0;
        $pTingkatUser = request()->input('tingkatUser');
        $userID = request()->input('userID');

        $this->treeAkses($result, $userID, $pTingkatUser);
        return $result;
    }

    public function treeAkses(&$result, $pUserID, $pTingkatUser, $parentid=0, $level='')
    {
        $tabelWilayah = [
            1 => ['table'=>'Provinsi', 'parent'=>'RegionalID'],
            2 => ['table'=>'Kabupaten', 'parent'=>'id_provinsi'],
            3 => ['table'=>'Kecamatan', 'parent'=>'id_kabupaten'],
            4 => ['table'=>'Kelurahan', 'parent'=>'id_kecamatan'],
            5 => ['table'=>'RW', 'parent'=>'id_kelurahan'],
            6 => ['table'=>'RT', 'parent'=>'id_rw'],
        ];
        if ($level=='') {
            $level = currentUser('TingkatWilayahID');
        }
        if ($level==0) {
            //PUSAT
            $item = [];
            $item['id'] = '0_0';
            $item['text'] = 'PUSAT';
            $item['level'] = $level;
            $item['parent'] = null;
            $item['isLeaf'] = true;
            $item['WilayahID'] = null;
            $item['Flag'] = 0;
            $item['TargetKK'] = null;
            $result[] = $item;
            $this->treeAkses($result, $pUserID, $pTingkatUser, '*', $level+1);
            $rows = collect([]);
        } else {
            if ($parentid===0) {
                $userID = currentUser('ID');
                $wilayahID = UserAkses::where('UserID', $userID)->pluck('WilayahID');
                $level = currentUser('TingkatWilayahID');
                $modelname = 'App\\Models\\Master\\'.$tabelWilayah[$level]['table'];
                $model = new $modelname;
                $rows = $model->getByID($wilayahID->toArray());
            } else {
                $modelname = 'App\\Models\\Master\\'.$tabelWilayah[$level]['table'];
                $model = new $modelname;
                $rows = $model->getByParent($parentid);
            }
        }
        $userAkses = [];
        if ($level==$pTingkatUser) {
            $modelAkses = new UserAkses();
            $userAkses = $modelAkses->where('UserID', $pUserID)->get()->toArray();
        }
        foreach ($rows->toArray() as $row) {
            $item = [];
            $item = (array)$row;
            $item['id'] = $level.'_'.$row['id'];
            $item['level'] = $level;
            $item['parent'] = ($level-1).'_'.$parentid;
            $haschildren = false;
            if ($level < $pTingkatUser) {
                $childmodelname = 'App\\Models\\Master\\'.$tabelWilayah[$level+1]['table'];
                $childmodel = new $childmodelname;
                $childrows = $childmodel->getByParent($row['id'])->count();
                $haschildren = $childrows > 0 ? true : false;
            }
            $item['isLeaf'] = ($level >= $pTingkatUser || !$haschildren) ? true : false;
            $item['WilayahID'] = $row['id'];
            $item['Flag'] = 0;
            $item['TargetKK'] = null;
            if ($level==$pTingkatUser) {
                foreach ($userAkses as $akses) {
                    if ($akses['WilayahID']==$row['id']) {
                        $item['Flag'] = 1;
                        $item['TargetKK'] = $level==5 || $level==6 ? $akses['TargetKK'] : '';
                    }
                }
            }
            $result[] = $item;
            if ($level<$pTingkatUser) {
                $this->treeAkses($result, $pUserID, $pTingkatUser, $row['id'], $level+1);
            }
        }
        return $result;
    }


    public function getProvinces($id)
    {
        //$wilayah = $this->whereRaw('CHAR_LENGTH(kode)=2')->orderBy('nama')->get();
        $sql = 'select * from "Provinsi" limit 100';
        $wilayah = DB::select($sql);
        return $wilayah;
    }


    public function getRegencies($id)
    {
       //$wilayah = $this->where('kode', 'LIKE', $id.'%')->whereRaw('CHAR_LENGTH(kode)=5')->orderBy('nama')->get();
        $sql = 'select id_kabupaten, nama_kabupaten from "Kabupaten" where id_provinsi='.$id.' order by 2 limit 200';
        $wilayah = DB::select($sql);
        return $wilayah;
    }

    public function getDistricts($id)
    {
        //$wilayah = $this->where('kode', 'LIKE', $id.'%')->whereRaw('CHAR_LENGTH(kode)=8')->orderBy('nama')->get();

        $sql = 'select id_kecamatan, nama_kecamatan from "Kecamatan" where id_kabupaten='.$id.' order by 2 limit 300';
        $wilayah = DB::select($sql);
        return $wilayah;
    }

    public function getVillages($id)
    {
        //$wilayah = $this->where('kode', 'LIKE', $id.'%')->whereRaw('CHAR_LENGTH(kode)=13')->orderBy('nama')->get();

        $sql = 'select id_kelurahan, nama_kelurahan from "Kelurahan" where id_kecamatan='.$id.' order by 2 limit 400';
        $wilayah = DB::select($sql);
        return $wilayah;
    }

    public function getDataTable()
    {
        $pegawai = DB::table('mst_formulir_dtl');
        return $pegawai;
    }

    public function getDataProvinsi()
    {
        $sql = 'select * from "Provinsi" limit 100';
        $data = DB::select($sql);
        return $data;
    }

    public function postAddRW()
    {
        $request = request()->all();
        try {
            $maxValue = DB::table('RW')->orderBy('id_rw', 'desc')->value('id_rw');
            $requests['id_rw'] = $maxValue + 1;

            $requests['id_kelurahan'] = $request['idkel'];
            $requests['nama_rw'] = $request['nama_rw'];
            $requests['OriginalNama'] = '-';
            $requests['KodeDepdagri'] = $request['KodeDepdagri'];

            $requests['Created'] = date('Y-m-d H:i:s');
            $requests['CreatedBy'] = currentUser('UserName');
            $requests['LastModified'] = date('Y-m-d H:i:s');
            $requests['LastModifiedBy'] = currentUser('UserName');
            $requests['IsActive'] = 't';

            //$rw = DB::table('RW')->find($id);
            //$rw->fill($requests);
            //$row = $rw->save();
            $rw = DB::table('RW')->insert($requests);

            // send reset password user email
            // $this->sendResetEmail($rw->ID, $request['Password']);

            $this->jsonResponse->status = true;
            $this->jsonResponse->message = 'Data berhasil disimpan';
        } catch (\Exception $e) {
            $this->jsonResponse->message = getExceptionMessage($e);
        }
        return $this->jsonResponse->get();
    }

    public function postEditRW()
    {
        $request = request()->all();
        try {
            $id = $request['id_rw'];
            $requests['nama_rw'] = $request['nama_rw'];
            $requests['LastModified'] = date('Y-m-d H:i:s');
            $requests['LastModifiedBy'] = currentUser('UserName');

            $rw = DB::table('RW')->where('id_rw', $id)->update($requests);

            $this->jsonResponse->status = true;
            $this->jsonResponse->message = 'Data berhasil disimpan, Silahkan Cek Ulang.!';
        } catch (\Exception $e) {
            $this->jsonResponse->message = getExceptionMessage($e);
        }
        return $this->jsonResponse->get();
    }

    public function postAddRT()
    {
        $request = request()->all();
        try {
            $maxValue = DB::table('RT')->orderBy('id_rt', 'desc')->value('id_rt');
            $maxValue2 = DB::table('RT')->orderBy('RTID', 'desc')->value('RTID');
            $requests['id_rt'] = $maxValue + 1;

            $requests['id_rw'] = $request['idRW'];
            $requests['nama_rt'] = $request['nama_rt'];

            $result = $this->getDataWilayahRW($request['idRW']);
            // loop data header RT -
            foreach ($result as $data) {
                $requests['KodeRW'] = $data->id_provinsi;
                $requests['NamaRW'] = $data->nama_rw;
                $requests['KelurahanID'] = $data->id_kelurahan;
                $requests['KodeKelurahan'] = $data->KodeDepdagriKelurahan;
                $requests['NamaKelurahan'] = $data->nama_kelurahan;

                $requests['KecamatanID'] = $data->id_kecamatan;
                $requests['KodeKecamatan'] = $data->KodeDepdagriKecamatan;
                $requests['NamaKecamatan'] = $data->nama_kecamatan;

                $requests['KabupatenID'] = $data->id_kabupaten;
                $requests['KodeKabupaten'] = $data->KodeDepdagriKabupaten;
                $requests['NamaKabupaten'] = $data->nama_kabupaten;

                $requests['ProvinsiID'] = $data->id_provinsi;
                $requests['KodeProvinsi'] = $data->KodeDepdagriProvinsi;
                $requests['NamaProvinsi'] = $data->nama_provinsi;
            }

            $requests['RTID'] = $maxValue2;
            $requests['KodeRT'] = $request['KodeRT'];
            ;

            $rw = DB::table('RT')->insert($requests);

            $this->jsonResponse->status = true;
            $this->jsonResponse->message = 'Data berhasil disimpan';
        } catch (\Exception $e) {
            $this->jsonResponse->message = getExceptionMessage($e);
        }
        return $this->jsonResponse->get();
    }



    public function postEditRT()
    {
        $request = request()->all();
        try {
            $id = $request['id_rt'];
            $requests['nama_rt'] = $request['nama_rt'];
            //$requests['LastModified'] = date('Y-m-d H:i:s');
            //$requests['LastModifiedBy'] = currentUser('UserName');

            $rw = DB::table('RT')->where('id_rt', $id)->update($requests);

            $this->jsonResponse->status = true;
            $this->jsonResponse->message = 'Data berhasil disimpan, Silahkan Cek Ulang.!';
        } catch (\Exception $e) {
            $this->jsonResponse->message = getExceptionMessage($e);
        }
        return $this->jsonResponse->get();
    }

    public function getDataWilayahRW($idrw)
    {
        $sql = 'select * from v_data_wilayah_rw where id_rw = \''.$idrw.'\' limit 1';

        $result = DB::select($sql);
        //$result = $result->toArray();
        return $result;
    }
}
