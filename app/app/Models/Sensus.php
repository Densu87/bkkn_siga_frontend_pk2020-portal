<?php

namespace App\Models;

use App\Libraries\DataTable;
use App\Libraries\jqGrid;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sensus extends Model
{
    //
    protected $table = 'v_datasensus';

    public function getAll()
    {
        //$blogs = Blog::all();
        //$data = $this::all();
        //$collection = $this::all();
        $collection = $this->orderby('no_kk', 'asc');

        $data = new jqGrid($collection, ['searchFields'=>['alamat1', 'alamat2', 'no_kk']]);
        $sensus = $data->get();
        return $sensus;
    }


    public function getDataTable()
    {
        $data = DB::table('v_datasensus');
        return $data;
    }

    public function getDataTableNotValid()
    {
        $data = DB::table('v_datasensus')->where('Code', '!=', '1')->count();
        return $data;
    }
}
