<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class RT extends Model
{
    protected $table = 'RT';
    protected $primaryKey = 'ID';
    
    public function getByID($id_rt=null)
    {
        $rows = $this->select('id_rt as id', 'nama_rt as text', 'RTID as kode');
        
        if (!empty($id_rt)) {
            if (is_array($id_rt)) {
                $rows = $rows->whereIn('id_rt', $id_rt);
            } else {
                $rows = $rows->where('id_rt', $id_rt);
            }
        }
        $rows = $rows->orderBy('nama_rt')->get();
        return $rows;
    }
    
    public function getByParent($id_rw=null)
    {
        $rows = $this->select('id_rt as id', 'nama_rt as text', 'RTID as kode');
        
        if (!empty($id_rw)) {
            if (is_array($id_rw)) {
                $rows = $rows->whereIn('id_rw', $id_rw);
            } else {
                $rows = $rows->where('id_rw', $id_rw);
            }
        }
        $rows = $rows->orderBy('nama_rt')->get();
        return $rows;
    }
}
