<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Provinsi;
use App\Models\Master\Kabupaten;
use App\Models\Master\Kecamatan;
use App\Models\Master\Kelurahan;
use App\Models\Master\RW;
use App\Models\Master\RT;

class UserAkses extends BaseModel
{   
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }
    
    protected $table = 'UserAkses';
    protected $primaryKey = 'ID';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'UserID', 'ID');
    }
    
    public function byUserID($userID)
    {
        $rows = $this->where('UserID', $userID)->get();
        return $rows;
    }
    
    
    public function postUpdateAkses()
    {
        
        try {
            $userID = request()->input('UserID');
            $tree = request()->input('tree');
            $targetKK = array_filter($tree, function($obj) {
                if ($obj['Flag']==1) {
                    return $obj;
                }
            });
            $data = [];
            
            $this->where('UserID', $userID)->delete();
            
            foreach ($tree as $row) {
                if ($row['Flag']==1) {
                    $maxValue = UserAkses::orderBy('ID', 'desc')->value('ID');
                    $item = [];
                    $item['ID'] = $maxValue+1;// \DB::raw("nextval('\"UserAkses_ID_seq\"')");;
                    $item['UserID'] = $userID;
                    $item['WilayahID'] = $row['WilayahID'];
                    $item['TargetKK'] = $row['TargetKK']??0;
                    $data[] = $item;
                    
                    $this->insert($item);
                }
            }
            
            
            $muser = new User();
            
            User::where('ID', $userID)->update(
                [
                    'LastModified' => date('Y-m-d H:i:s'),
                    'LastModifiedBy' => currentUser('UserName')
                ]
            );
            
            // send new user email
            $muser->sendNewUserEmail($userID);
            
            $this->jsonResponse->data = $data;
            $this->jsonResponse->status = true;
            $this->jsonResponse->message = 'Data berhasil disimpan';
        } catch (\Exception $e) {
            $this->jsonResponse->message = getExceptionMessage($e);
        }
        return $this->jsonResponse->get();
    }
    
}
