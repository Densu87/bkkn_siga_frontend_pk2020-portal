/**
 * Custom jqGrid JS
**/

$.fn.searchGrid = function(grid){
    this.keyup(function(){
        var searchText = $(this).val();
        if (searchText.length < 2) return false;
        $('#btSearchGrid').addClass('alert-danger').html('<i class="fa fa-undo"></i>');
        delay(function(){
            var fields = [];
            var colModel = $(grid).jqGrid("getGridParam", "colModel");
            for (i = 0; i < colModel.length; i++) {
                cm = colModel[i];
                if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                    fields.push(cm.name);
                }
            }
            var postData = $(grid).jqGrid("getGridParam", "postData");
            postData.strsearch = searchText;
            postData.fieldsearch = fields.join();
            $(grid).jqGrid("setGridParam", { search: true});
            $(grid).trigger("reloadGrid", [{page: 1, current: true}]);
        }, 750);
    });
}
function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    console.log(ms);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}

$.jgrid.extend({
    searchbox: function (searchID){
        var par = this.jqGrid('getGridParam');
        
        $(document).on('keyup',searchID, function(evt){
            var searchText = $(searchID).val();
                // console.log(searchID, evt.keyCode);
            //if (searchText.length < 2) return false;
            //$('#btSearchGrid').addClass('alert-danger').html('<i class="fa fa-undo"></i>');
            delay(function(evt){
                console.log(searchText, evt.keyCode);
                // var fields = [];
                // var colModel = $(grid).jqGrid("getGridParam", "colModel");
                // for (i = 0; i < colModel.length; i++) {
                    // cm = colModel[i];
                    // if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        // fields.push(cm.name);
                    // }
                // }
                // var postData = $(grid).jqGrid("getGridParam", "postData");
                // postData.strsearch = searchText;
                // postData.fieldsearch = fields.join();
                // $(grid).jqGrid("setGridParam", { search: true});
                // $(grid).trigger("reloadGrid", [{page: 1, current: true}]);
            }, 750);
        });
        // console.log(par);
        // $(document).on('keypress',searchID, function(evt){
            // console.log(searchID, evt.keyCode);
            
        // });
        // var opsi = null;
        // var defaults = {
            // btnCreate: false,
            // btnCreateTitle: 'Tambah'
        // };
        // if (typeof options != undefined)
        // {
            // for (var defproperty in defaults) {
                // if (defaults.hasOwnProperty(defproperty)) {
                    // if (typeof options != 'undefined' && options.hasOwnProperty(defproperty)) {
                        // defaults[defproperty] =  options[defproperty];
                    // }
                // }
            // }
        // }
        
        
        
        // var elbtnCreate = '';
        // if (defaults.btnCreate){
            // elbtnCreate = '    <button type="button" class="btn btn-primary" id="btnCreate">'+defaults.btnCreateTitle+'</button>';
        // }
        // var grid = this;    
        // var el = '<div class="row mb-1">'+
            // '<div class="col-sm-6">'+
            // elbtnCreate +
            // '</div>'+
            // '<div class="col-sm-6 text-right">'+
            // '    <div class="input-group float-right" style="width:auto">'+
            // '        <input type="text" id="gridsearch" class="form-control" placeholder="Search">'+
            // '        <span class="input-group-addon" id="btSearchGrid"><i class="fa fa-search"></i>'+
            // '        </span>'+
            // '    </div>'+
            // '</div>'+
        // '</div>';
        // gridID = grid[0].id;
        // var wrapper = '#gbox_'+ gridID;
        // $(el).insertBefore(wrapper);
        // $('#gridsearch').searchGrid('#'+gridID);
    }
});