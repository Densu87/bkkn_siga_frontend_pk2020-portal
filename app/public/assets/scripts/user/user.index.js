var UserIndex = function() {
	var initTable = function() {
		$('#tableroles').jqGrid({
			datatype: 'json',
			url: base_url + '/user/datapaging',
			pager: '#pagerroles',
			shrinkToFit: pShrinkToFit,
			forceFit: pForceFit,
			sortable: true,
			viewrecords: true,
			rownumbers: true,
			autowidth: true,
			rowNum: 10,
            loadonce: true,
			rowList: [5, 10, 20],
			colModel: [{
				label: 'ID',
				name: 'ID',
                key:true, 
                hidden: true,
			}, {
				name: 'UserName',
				label: 'Kode User',
                searchoptions: {sopt:['cn']},
			}, {
				name: 'NamaLengkap',
				label: 'Nama Lengkap',
                searchoptions: {sopt:['cn']},
			}, {
				name: 'Email',
				label: 'Email', 
                search: false
			}, {
				name: 'NoTelepon',
				label: 'HP',  
                search: false
			}, {
				name: 'Alamat',
				label: 'Alamat', 
                search: false
			}, {
				name: 'RoleID',
				label: 'Role ID', 
                search: false,
                hidden: true,
			},  {
				name: 'role.RoleName', 
                search: false,
				label: 'Role',
			}, {
				name: 'TingkatWilayahID',
				label: 'Tingkat Wilayah ID',
                hidden: true, 
                search: false
			}, {
				name: 'TingkatWilayah',
				label: 'Tingkat Wilayah', 
                search: false
			}, {
				label: 'Wilayah',
				align: 'center', 
                search: false,
				width: 70,
				formatter: function(val, opt, row) {
					var cnt = row.cntwil;//row.akses.length;
                    var btnClass = cnt > 0 ? ' btn-outline-primary ':' btn-outline-danger ';
                    var btnTitle = cnt > 0 ? ' Pengaturan Wilayah ':' Pengaturan Wilayah belum ditetapkan ';
					var btnWil = '<button type="button" title="'+btnTitle+'" class="btn '+btnClass+' btn-elevate btn-icon btn-sm btnWil" data-id="' + opt.rowId + '"><i class="fas fa-globe-asia"></i></button>&nbsp;';
					return btnWil;
				}
			}, {
				label: 'Action',
				align: 'center',
                search: false,
				formatter: function(val, opt, row) {
					var btnEdit = '<button type="button" title="Edit User &quot;' + row.UserName + '&quot;" class="btn btn-outline-success btn-elevate btn-icon btn-sm btnEdit" data-id="' + row.ID + '"><i class="fa fa-user-edit"></i></button>&nbsp;';
					var btnReset = '<button type="button" title="Reset Password  &quot;' + row.UserName + '&quot;" class="btn btn-outline-warning btn-elevate btn-icon btn-sm btnReset" data-id="' + row.ID + '"><i class="fa fa-users-cog"></i></button>&nbsp;';
					var btnDelete = '<button type="button" title="Non-aktifkan User &quot;' + row.UserName + '&quot;" class="btn btn-outline-danger btn-elevate btn-icon btn-sm btnDelete" data-id="' + row.ID + '"><i class="fa fa-user-times"></i></button>&nbsp;';
					return btnEdit + btnReset + btnDelete;
				}
			}, ],
		});

        $('#tableroles').jqGrid('filterToolbar', { searchoperators: false, stringResult: true });

	}

	var btnCreateHandler = function() {
		$('#btnCreate').click(function() {
			$('#modalRoleCreate').modal();
		});
	}

	var btnImportHandler = function() {
		$('#btnImport').click(function() {
			window.location.href = base_url+'/user/import';
		});
	}

	var btnEditHandler = function() {
		$(document).on('click', '.btnEdit', function() {
			var selRowId = $(this).data('id');
			var row = $('#tableroles').jqGrid('getRowData', selRowId);
			$('#formUserEdit #ID').val(row.ID);
			$('#formUserEdit #UserName').val(row.UserName);
			$('#formUserEdit #Email').val(row.Email);
			$('#formUserEdit #NamaLengkap').val(row.NamaLengkap);
			$('#formUserEdit #Alamat').val(row.Alamat);
			$('#formUserEdit #NoTelepon').val(row.NoTelepon);
			$('#modalUserEdit ').modal();
		});
	}


	var formUpdate = function() {
		$('#btnUpdateUser').click(function(e) {
			e.preventDefault();
			KTApp.block('#modalUserEdit', {
				message: 'Harap tunggu...'
			});

			var disabled = $('#formUserEdit').find(':input:disabled').removeAttr('disabled');

			var id = $('#formUserEdit #ID').val();
			var param = $('#formUserEdit').serializeObject()
			disabled.attr('disabled', 'disabled');

			var header = {
				'Accept': 'application/json',
				'X-CSRF-TOKEN': csrf_token,
			};

			var xhr = $.ajax({
					url: base_url + '/user/' + id,
					method: 'PUT',
					dataType: 'json',
					headers: header,
					data: param
				})
				.done(function(response) {
					var message = (response.message) ? response.message : '';
					if (response.status) {
						toastr.options.onHidden = formClear;
						toastr.success(message);
					} else {
						message = (message !== '') ? message : 'Ada kesalahan';
						swal.fire('', message, 'error');
					}
				}).fail(function(jqXHR, textStatus, errorThrown) {
					var message = (typeof jqXHR.responseJSON === 'undefined') ? errorThrown : jqXHR.responseJSON.message;
					swal.fire('', message, 'error');
				}).always(function() {
					KTApp.unblock('#modalUserEdit');
				});
		});
	}

	var formSave = function() {
		$('#btnSave').click(function(e) {
			e.preventDefault();
			KTApp.block('#modalRoleCreate', {
				message: 'Harap tunggu...'
			});
			var param = {
				RoleName: $('#formUserCreate #name').val(),
			};

			var disabled = $('#formUserEdit').find(':input:disabled').removeAttr('disabled');
            
			var param = $('#formUserCreate').serializeObject()
			disabled.attr('disabled', 'disabled');

			var header = {
				'Accept': 'application/json',
				'X-CSRF-TOKEN': csrf_token,
			};

			var xhr = $.ajax({
					url: base_url + '/user',
					method: 'POST',
					dataType: 'json',
					headers: header,
					data: param
            })
            .done(function(response) {
                var message = (response.message) ? response.message : '';
                if (response.status) {
                    toastr.options.onHidden = formClear;
                    toastr.success(message);
                } else {
                    message = (message !== '') ? message : 'Ada kesalahan';
                    swal.fire('', message, 'error');
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                var message = (typeof jqXHR.responseJSON === 'undefined') ? errorThrown : jqXHR.responseJSON.message;
                swal.fire('', message, 'error');
            }).always(function() {
                KTApp.unblock('#modalRoleCreate');
            });
		});
	}


	var formClear = function() {
		$('#formRoleCreate, #formUserEdit, #formPasswordReset').trigger('reset');
		$('#modalRoleCreate, #modalUserEdit, #modalResetCreate').modal('hide');
		$('#tableroles').trigger('reloadGrid');
	}


	var btnResetHandler = function() {
		$(document).on('click', '.btnReset', function() {
			var selRowId = $(this).data('id');
			var row = $('#tableroles').jqGrid('getRowData', selRowId);
			$('#formPasswordReset #ID').val(row.ID);
			$('#modalResetCreate').modal();
			$('#modalResetCreate #Password').val('');
			$('#modalResetCreate #rePassword').val('');
		});
	}


	var formReset = function() {
		$('#btnResetSave').click(function(e) {
			e.preventDefault();
			KTApp.block('#modalResetCreate', {
				message: 'Harap tunggu...'
			});

			var disabled = $('#modalResetCreate').find(':input:disabled').removeAttr('disabled');

			var id = $('#formPasswordReset #ID').val();
			var param = $('#formPasswordReset').serializeObject()
			disabled.attr('disabled', 'disabled');

			var header = {
				'Accept': 'application/json',
				'X-CSRF-TOKEN': csrf_token,
			};

			var xhr = $.ajax({
					url: base_url + '/user/resetPassword/' + id,
					method: 'PUT',
					dataType: 'json',
					headers: header,
					data: param
				})
				.done(function(response) {
					var message = (response.message) ? response.message : '';
					if (response.status) {
						toastr.options.onHidden = formClear;
						toastr.success(message);
					} else {
						message = (message !== '') ? message : 'Ada kesalahan';
						swal.fire('', message, 'error');
					}
				}).fail(function(jqXHR, textStatus, errorThrown) {
					var message = (typeof jqXHR.responseJSON === 'undefined') ? errorThrown : jqXHR.responseJSON.message;
					swal.fire('', message, 'error');
				}).always(function() {
					KTApp.unblock('#modalResetCreate');
				});
		});
	}

	var btnWilHandler = function() {
		$(document).on('click', '.btnWil', function() {
			var id = $(this).data('id');
			$('#modalWilayah').modal();
		});
	}


	var btnDeleteHandler = function() {
		$(document).on('click', '.btnDelete', function() {
			var userID = $(this).data('id');

			var header = {
				'Accept': 'application/json',
				'X-CSRF-TOKEN': csrf_token,
			};
            
            swal.fire({
                title: '',
                text: 'Yakin akang menong-aktifkan user ini?',
                icon: 'warning',
                showCancelButton: true,
            }).then((result) => {
                if (result.value) {
                    var xhr = $.ajax({
                            url: base_url + '/user/'+userID,
                            method: 'DELETE',
                            dataType: 'json',
                            headers: header
                    })
                    .done(function(response) {
                        var message = (response.message) ? response.message : '';
                        if (response.status) {
                            toastr.options.onHidden = formClear;
                            toastr.success(message);
                        } else {
                            message = (message !== '') ? message : 'Ada kesalahan';
                            swal.fire('', message, 'error');
                        }
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        var message = (typeof jqXHR.responseJSON === 'undefined') ? errorThrown : jqXHR.responseJSON.message;
                        swal.fire('', message, 'error');
                    }).always(function() {
                    });
                }
            })
            
		});
	}

	var select2Handler = function() {
		$('#RoleID').select2({
            placeholder: '--- Pilih Role ---',
        }).on('change', function(e) {
			getWilayah();
		});
        
        
		$('#TingkatWilayahID').select2({
            placeholder: '--- Pilih Tingkat Wilayah ---',
        });
	}

	var getRole = function() {
		var xhr = $.ajax({
				url: base_url + '/user/getrole',
				method: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				var el = $('#RoleID');
				el.children().remove();
				el.append($("<option></option>").attr("value", '').text(''));
				$.each(response, function(key, value) {
					el.append($("<option></option>").attr("value", value.ID).text(value.RoleName));
				});

			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				toastr.error('getRole: ' + jqXHR.statusText);
			})
			.always(function() {});
	}

	var getWilayah = function() {
        var roleid = $('#RoleID').val();
		var xhr = $.ajax({
				url: base_url + '/user/wilayah/'+roleid,
				method: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				var el = $('#TingkatWilayahID');
				el.children().remove();
				el.append($("<option></option>").attr("value", '').text(''));
				$.each(response, function(key, value) {
					el.append($("<option></option>").attr("value", value.ID).text(value.TingkatWilayah));
				});
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				toastr.error('getWilayah: ' + jqXHR.statusText);
			})
			.always(function() {});
	}

	return {
		init: function() {
			initTable();
			btnCreateHandler();
			formSave();

			btnEditHandler();
			formUpdate();

			btnResetHandler();
			formReset();
			btnWilHandler();

			select2Handler();
			getRole();

			btnDeleteHandler();
			btnImportHandler();
		}
	};
}();

$(document).ready(function() {
	UserIndex.init();
});