var UserWilayah = function () {
    
    var editingID = null;
    var selectedUser = null;
    var checkedArea = [];
    var grid;
    
    var btnWilHandler = function () {
        $(document).on('click', '.btnWil', function(){
            var rowid = $(this).data('id');
            selectedUser = $('#tableroles').jqGrid('getRowData', rowid);
            if (selectedUser.TingkatWilayahID=='' || typeof selectedUser.TingkatWilayahID==='undefined' ) {
                swal.fire('', 'Tingkat wilayah user belum ditetapkan', 'error');
                return false;
            }
            $('#modalWilayah').modal();
        });
    }
    
    var modalWilayahCallback = function () {
        $('#modalWilayah').on('shown.bs.modal', function (e) {
            $('#formUserAkses #UserID').val(selectedUser.ID);
            $('#formUserAkses #TingkatWilayahIDUser').val(selectedUser.TingkatWilayahID);
            $('#formUserAkses #TingkatWilayahUser').html(selectedUser['TingkatWilayah']);
            initTree();
        });
    }
    
    var initTree = function () {
        
        var pUserID = $('#formUserAkses #UserID').val();
        var pTingkatUser = $('#formUserAkses #TingkatWilayahIDUser').val();
        if (grid) grid.jqGrid("GridUnload");
        grid = $('#treewilayah').jqGrid({
            datatype: 'json',
            url: base_url+'/wilayah/akses',
            postData: {
                userID: pUserID,
                tingkatUser: pTingkatUser,
            },
            pager: '#pagerwilayah',
			shrinkToFit: pShrinkToFit,
			forceFit: pForceFit,
            autowidth: true,
            
            treeGrid: true,
            treeGridModel: 'adjacency',
            ExpandColumn: 'text',
            treedatatype: 'json',
            
            colModel: [
                {
                    label: 'ID',
                    name: 'id',
                    hidden: true,
                }, { 
                    label: 'WilayahID', 
                    name: 'WilayahID',
                    hidden: true,
                }, { 
                    label: 'Wilayah', 
                    name: 'text',
                }, { 
                    label: 'Pilih', 
                    name: 'Pilih', 
                    align: 'center',
                    formatter: function(val, opt, row){
                        var check = "&nbsp;";
                        if (row.level>=pTingkatUser) {
                            var checked = row.Flag==1?' checked="checked" ':' ';
                            check = '<label class="kt-checkbox kt-checkbox--brand"><input type="checkbox" '+checked+' class="checkPilih" data-id="'+row.id+'">&nbsp;<span></span></label>&nbsp;';
                        }
                        return check;
                    }
                }, { 
                    label: 'Flag', 
                    name: 'Flag',
                    hidden: true,
                },  { 
                    label: 'Target KK', 
                    name: 'TargetKK',
                    formatter: 'integer',
                    formatoptions: {defaultValue:''}, 
                    editable: true,
                    edittype: 'text',
                },
            ],
            onCellSelect: function(rowid, iCol, val, e) {
                endEdit(editingID);
                var row = $(this).jqGrid('getRowData', rowid);
                if (row.Flag==1) {
                    editingID = rowid;
                    startEdit(editingID);
                }
            }
        });

    }
    
    var startEdit = function (rowid) {
        if (selectedUser.RoleID==5) {
        grid.jqGrid(
            'editRow', 
            rowid, 
            { 
                key: true
            }
        );
        }
    }
    
    var endEdit = function (rowid) {
        if (selectedUser.RoleID==5) {
        grid.jqGrid(
            'saveRow', 
            rowid
        );
        }
    }
    
    var checkboxHandler = function () {
        $(document).on('click', '.checkPilih', function(){
            endEdit(editingID)
            var rowid = $(this).data('id');
            var row = $('#treewilayah').jqGrid('getRowData', rowid);
            if ($(this).is(':checked')) {
                if ((selectedUser.TingkatWilayahID==5 || selectedUser.TingkatWilayahID==6 )&& (row.TargetKK=='' || typeof row.TargetKK === 'undefined')) {
                    $('#treewilayah').jqGrid('setRowData', rowid, {TargetKK:0});
                }
                $('#treewilayah').jqGrid('setRowData', rowid, {Flag:1});
                editingID = rowid;
                startEdit(editingID);
            } else {
                if (row.TargetKK==0) {
                    $('#treewilayah').jqGrid('setRowData', rowid, {TargetKK:''});
                }
                $('#treewilayah').jqGrid('setRowData', rowid, {Flag:0});
            }
        });
    }
    
    var checkTargetKKVal = function(treedata) {
        var MaxTarget = $('#MaxTarget').val();
        var MinTarget = 0;
        var result = false;
        var data = null;
        var maxExceed = false;
        var output = {};
        var checked = treedata.filter(function (i){
            if (selectedUser.RoleID==5 && i.Flag=='1' && (parseInt(i.TargetKK) < parseInt(MinTarget) || parseInt(i.TargetKK) > parseInt(MaxTarget))) {
                maxExceed = true;
            }
            return i.Flag==='1';
        });
        
        if (checked.length > 0) {
            if (maxExceed) {
                data = 'Target KK tidak boleh melebihi batas: '+MaxTarget;
            } else {
                result = true;
                data = checked;
            }
        } else {
            data = 'Akses Wilayah belum dipilih';
        }
        output.result = result;
        output.data = data;
        return output;
    }
    
    var checkMaxWilayah = function(treedata) {
        var Is1Kelurahan = $('#Is1Kelurahan').val() === '1' ? true : false;
        var result = false;
        var data = null;
        var output = {};
        var checked = treedata;
        if (checked.length > 0) {
            if (checked.length > 1 && Is1Kelurahan && selectedUser.RoleID<5 ) {
                data = 'User hanya diperbolehkan memiliki 1 Akses Wilayah';
            } else {
                result = true;
                data = checked;
            }
        } else {
            data = 'Akses Wilayah belum dipilih';
        }
        output.result = result;
        output.data = data;
        return output;
    }
    
    var formSave = function () {
        $('#btnUpdateAkses').click(function (e) {
            e.preventDefault();
            endEdit(editingID);
            var formdata = $('#formUserAkses').serializeObject();
            var treedata = $('#treewilayah').jqGrid('getRowData');
            var aksesdata = [];
            aksesdata = checkTargetKKVal(treedata);
            if (!aksesdata.result) {
                swal.fire('', aksesdata.data, 'error');
                return false;
            }
            
            aksesdata = checkMaxWilayah(aksesdata.data);
            if (!aksesdata.result) {
                swal.fire('', aksesdata.data, 'error');
                return false;
            }
            formdata.tree = aksesdata.data;
            var header = {
                'Accept': 'application/json',
                'X-CSRF-TOKEN': csrf_token,
            };
            
            var xhr = $.ajax({
                url: base_url+'/wilayah/update',
                method: 'POST',
                dataType: 'json',
                headers: header,
                data: formdata
            })
            .done(function(response){
                message = (response.message) ? response.message : '';
                if (response.status) {
                    toastr.options.onHidden = function() {
                        $('#modalWilayah').modal('hide');
                    };
                    toastr.success(message);
                } else {
                    message = (message !== '') ? message :  'Ada kesalahan';
                    swal.fire('', message, 'error');
                }
            }).fail(function(jqXHR, textStatus, errorThrown){
                swal.fire('', jqXHR.statusText, 'error');
            }).always(function() {
                KTApp.unblockPage();
            });
        });
    }
    
    return {
        init: function () {
            btnWilHandler();
            checkboxHandler();
            modalWilayahCallback();
            formSave();
        },
        
    };
}();

$(document).ready(function () {
    UserWilayah.init();
});