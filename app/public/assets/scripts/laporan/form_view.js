// "use strict";

var FormView = function() {    
    var id_frm;
    
    var grid;
    
    var initTable = function (param) {
        $('#tableview').jqGrid("GridUnload");
        grid = $('#tableview').jqGrid({
                datatype: 'json',
                mtype: 'GET',
                url: base_url+'/formulir/demografi/'+id_frm,
                postData: param,
                // pager: '#pagerview',
                shrinkToFit: false,
                forceFit: true,
                // sortable: true,
                headertitles: true,
                // viewrecords: true,
                // rownumbers: true,
                autowidth: true,
                rowNum: 100,
                // rowList: [5, 10, 20],
                colModel: [
                        { 
                            label: 'id_frm', 
                            name: 'id_frm',
                        }, { 
                            label: 'Nomor Anggota Keluarga', 
                            name: 'no_urutnik',
                        }, { 
                            label: 'Nama Anggota Keluarga/NIK', 
                            name: 'nama_anggotakel',
                            formatter: function(val, opt, row) {
                                return '<p>'+row.nama_anggotakel+'</p><p>'+row.nik+'</p>';
                            }
                        }, { 
                            label: 'Jenis Kelamin (Kode)', 
                            name: 'jenis_kelamin',
                        }, { 
                            label: 'Tanggal/Bulan/Tahun Lahir', 
                            name: 'tgl_lahir',
                        }, { 
                            label: 'Status Perkawinan (Kode)', 
                            name: 'sts_kawin',
                        }, { 
                            label: 'Jika kolom (7) berkode bukan 01, maka usia kawin pertama (tahun)', 
                            name: 'usia_kawin',
                            formatter: function(val, opt, row) {
                                var usia_kawin = row.sts_kawin==1 ? '' : row.usia_kawin;
                                if(usia_kawin === null) {
                                    usia_kawin = '';
                                }
                                return usia_kawin;
                            }
                        }, { 
                            label: 'Memiliki Akta Lahir (Kode)', 
                            name: 'status_akta',
                        }, { 
                            label: 'Hubungan dengan Kepala Keluarga (Kode)', 
                            name: 'sts_hubungan',
                        }, {
                            label: 'Hubungan Anak dengan Ibu (Kode)',
                            name: 'sts_hubanak_ibu',
                        }, {
                            label: 'Kode Ibu Kandung (Dilihat dari Nomor Anggota Keluarga)',
                            name: 'kd_ibukandung',
                        }, {
                            label: 'Agama yang dianut (Kode)',
                            name: 'id_agama',
                        }, {
                            label: 'Pekerjaan Utama (Kode)',
                            name: 'id_pekerjaan',
                        }, {
                            label: 'Pendidikan (Kode)',
                            name: 'jns_pendidikan',
                        }, {
                            label: 'Kepesertaan JKN/Asuransi Kesehatan lainnya (Kode)',
                            name: 'jns_asuransi',
                        },
                ],
            });

    }
    
	var getKB1Form = function() {
        KTApp.block('#kb1form', {message: 'Harap tunggu...'});
		var xhr = $.ajax({
				url: base_url + '/formulir/kb1form/'+id_frm,
				method: 'GET',
				dataType: 'html',
			})
			.done(function(response) {
                $('#kb1form').html(response);

			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log('jqXHR', jqXHR);
				toastr.error('getKB1Form: ' + jqXHR.statusText);
			})
			.always(function() {
                KTApp.unblock('#kb1form');
            });
	}
    
	var getPK01Form = function() {
        KTApp.block('#pk01form', {message: 'Harap tunggu...'});
		var xhr = $.ajax({
				url: base_url + '/formulir/pk01form/'+id_frm,
				method: 'GET',
				dataType: 'html',
			})
			.done(function(response) {
                $('#pk01form').html(response);

			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log('jqXHR', jqXHR);
				toastr.error('getPK01Form: ' + jqXHR.statusText);
			})
			.always(function() {
                KTApp.unblock('#pk01form');
            });
	}
    
	var getPK02Form = function() {
        KTApp.block('#pk02form', {message: 'Harap tunggu...'});
		var xhr = $.ajax({
				url: base_url + '/formulir/pk02form/'+id_frm,
				method: 'GET',
				dataType: 'html',
			})
			.done(function(response) {
                $('#pk02form').html(response);

			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log('jqXHR', jqXHR);
				toastr.error('getKB1Form: ' + jqXHR.statusText);
			})
			.always(function() {
                KTApp.unblock('#pk02form');
            });
	}
    

	var btnViewHandle = function() {
		$(document).on('click', '.btnView', function(e) {
            e.preventDefault();
			id_frm = $(this).data('id_frm');
			$('#modalDetail ').modal();
		});
	}
    
    var modalShowCallback = function () {
        $('#modalDetail').on('shown.bs.modal', function (e) {
            $('.nav-tabs a[data-target="#tab_demografi"]').tab('show');
            initTable();
            getKB1Form();
            getPK01Form();
            getPK02Form();
        });
    }
    
	return {
		init: function() {
            // initTable();
            btnViewHandle();
            modalShowCallback();
            // getKB1Form();
            // getPK01Form();
            // getPK02Form();
		}
	};
}();

jQuery(document).ready(function() {
	FormView.init();
});