// "use strict";

var StatusValid = function() {
    
    var grid;
    
    var initTableValid = function (param) {
        $('#tablemon').jqGrid("GridUnload");
        grid = $('#tablemon').jqGrid({
                datatype: 'json',
                mtype: 'GET',
                url: base_url+'/laporan/statussensus/data',
                postData: param,
                pager: '#pagermon',
                shrinkToFit: false,
                forceFit: true,
                sortable: true,
                sortname: 'create_date',
                sortorder: 'asc',
                viewrecords: true,
                rownumbers: true,
                autowidth: true,
                rowNum: 10,
                rowList: [5, 10, 20],
                colModel: [
                        { 
                            label: 'No. KK', 
                            name: 'no_kk',
                        }, { 
                            label: 'Alamat', 
                            name: 'alamat1',
                        }, { 
                            label: 'No. Urut Rumah', 
                            name: 'no_urutrmh',
                        }, { 
                            label: 'Kelurahan', 
                            name: 'nama_kelurahan',
                        }, { 
                            label: 'RW', 
                            name: 'nama_rw',
                        }, { 
                            label: 'RT', 
                            name: 'nama_rt',
                        }, {
                            label: 'Pendata',
                            name: 'UserName',
                        }, {
                            label: 'Tgl. Dibuat',
                            name: 'create_date',
                        }, { 
                            label: 'View',
                            align: 'center',
                            formatter: function(val, opt, row){
                                var btnView = '<button type="button" title="View Data Detail" class="btn btn-outline-brand btn-icon btn-sm btnView" data-id_frm="'+row.id_frm+'"><i class="fa fa-file-alt"></i></button>&nbsp;';
                            return btnView;
                            }
                        },
                ],
            });

    }
    
    var initTableInvalid = function (param) {
        $('#tablemon').jqGrid("GridUnload");
        grid = $('#tablemon').jqGrid({
                datatype: 'json',
                mtype: 'GET',
                url: base_url+'/laporan/statussensus/data',
                postData: param,
                pager: '#pagermon',
                shrinkToFit: false,
                forceFit: true,
                sortable: true,
                sortname: 'create_date',
                sortorder: 'asc',
                viewrecords: true,
                rownumbers: true,
                autowidth: true,
                rowNum: 10,
                rowList: [5, 10, 20],
                colModel: [
                        { 
                            label: 'No. KK', 
                            name: 'no_kk',
                        }, { 
                            label: 'Alamat', 
                            name: 'alamat1',
                        }, { 
                            label: 'No. Urut Rumah', 
                            name: 'no_urutrmh',
                        }, { 
                            label: 'Kelurahan', 
                            name: 'nama_kelurahan',
                        }, { 
                            label: 'RW', 
                            name: 'nama_rw',
                        }, { 
                            label: 'RT', 
                            name: 'nama_rt',
                        }, {
                            label: 'Pendata',
                            name: 'UserName',
                        }, {
                            label: 'Alasan',
                            name: 'alasan_text',
                        }, {
                            label: 'Tgl. Dibuat',
                            name: 'create_date',
                        }, { 
                            label: 'View',
                            align: 'center',
                            formatter: function(val, opt, row){
                                var btnView = '<button type="button" title="View Data Detail" class="btn btn-outline-brand btn-icon btn-sm btnView" data-id_frm="'+row.id_frm+'"><i class="fa fa-file-alt"></i></button>&nbsp;';
                            return btnView;
                            }
                        }, { 
                            label: 'Action',
                            align: 'center',
                            formatter: function(val, opt, row){
                                var btnView = '<button type="button" title="View Data Detail" class="btn btn-outline-brand btn-icon btn-sm btnAnulir text-danger" data-id="'+opt.rowId+'"><i class="fas fa-ban"></i></button>&nbsp;';
                            return btnView;
                            }
                        },
                ],
            });

    }
    
    var showReport = function () {
        $('#btnShow').click(function (e) {
            e.preventDefault();
        
            if ($('#PeriodeSensus').val()=='') {
                swal.fire('', 'Periode Sensus belum dipilih', 'error');
                return false;
            }
            
            if ($('#JenisData').val()=='') {
                swal.fire('', 'Jenis Data belum dipilih', 'error');
                return false;
            }
            var param = {
                StatusSensus: $('#StatusSensus').val(),
                PeriodeSensus: $('#PeriodeSensus').val(),
                JenisData: $('#JenisData').val(),
                Kelurahan: $('#Kelurahan').val(),
                RW: $('#RW').val(),
                RT: $('#RT').val(),
                Pendata: $('#Pendata').val(),
            };
            if ($('#StatusSensus').val()==1) {
                initTableValid(param);
            } else if ($('#StatusSensus').val()==2) {
                initTableInvalid(param);
            } else if ($('#StatusSensus').val()==4) {
                initTableValid(param);
            } 
        });
    }

	var showDetail = function() {
		$(document).on('click', '.btnView', function() {
			// var selRowId = $('#tableroles').jqGrid('getGridParam', 'selrow');
			// var row = $('#tableroles').jqGrid('getRowData', selRowId);
			// console.log(row);
			// $('#formUserEdit #ID').val(row.ID);
			// $('#formUserEdit #UserName').val(row.UserName);
			// $('#formUserEdit #Email').val(row.Email);
			// $('#formUserEdit #NamaLengkap').val(row.NamaLengkap);
			// $('#formUserEdit #Alamat').val(row.Alamat);
			// $('#formUserEdit #NoTelepon').val(row.NoTelepon);
			//$('#formUserEdit #Level').val(row.Level ? row.Level : 0);
			$('#modalDetail ').modal();
		});
	}

	var anulirData = function() {
		$(document).on('click', '.btnAnulir', function() {
            var rowId = $(this).data('id');
            swal.fire({
                  title: '',
                  text: "Yakin akan menganulir data berikut?",
                  showCancelButton: true,
                  confirmButtonText: 'Ya',
                  cancelButtonText: 'Batal'
                }).then((result) => {
                  if (result.value) {
                        var row = grid.jqGrid('getRowData', rowId);
                        var id = row.no_kk;
                        KTApp.blockPage({message: 'Harap tunggu...'});
                        
                        
                        var header = {
                            'Accept': 'application/json',
                            'X-CSRF-TOKEN': csrf_token,
                        };
                        
                        var xhr = $.ajax({
                            url: base_url+'/laporan/statussensus/anulir/'+id,
                            method: 'PUT',
                            dataType: 'json',
                            headers: header,
                        })
                        .done(function(response){
                            console.log(response.message);
                            message = (response.message) ? response.message : '';
                            if (response.status) {
                                toastr.options.onHidden = initTableInvalid();
                                toastr.success(message);
                            } else {
                                message = (message !== '') ? message :  'Ada kesalahan';
                                swal.fire('', message, 'error');
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown){
                            console.log('jqXHR', jqXHR);
                            swal.fire('', jqXHR.statusText, 'error');
                        }).always(function() {
                            KTApp.unblockPage();
                        });
                  }
                })
		});
	}
    
    var btnPrintHandler = function () {
        $('#btnPrint').click(function (e) {
            e.preventDefault();
            if ($('#PeriodeSensus').val()=='') {
                swal.fire('', 'Periode Pendataan belum dipilih', 'error');
                return false;
            }
            
            var param = {
                StatusSensus: $('#StatusSensus').val(),
                PeriodeSensus: $('#PeriodeSensus').val(),
                JenisData: $('#JenisData').val(),
                Kelurahan: $('#Kelurahan').val(),
                RW: $('#RW').val(),
                RT: $('#RT').val(),
                Pendata: $('#Pendata').val(),
                print: 1,
            };
            
            var querystr = $.param( param );
            var url = base_url + '/laporan/statussensus/data?' + querystr;
            // window.location.href = url;
            // window.open(url, '_blank');
            $('<a href="'+url+'" target="_blank">&nbsp;</a>')[0].click();

        });
    }
    
	return {
		init: function() {
            showReport();
            // showDetail();
            anulirData();
            btnPrintHandler();
		}
	};
}();

jQuery(document).ready(function() {
	StatusValid.init();
});