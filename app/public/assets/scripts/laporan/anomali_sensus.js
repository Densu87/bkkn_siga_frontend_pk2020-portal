// "use strict";

var AnomaliSensus = function() {
	var a = '';
	var b = '';
	var q = '';
	var message = '';
	var id_frm;
	var globalparam;

	var select2Handler = function() {

		$('#Indikator').select2({
			minimumResultsForSearch: -1,
		})

		$('#Kelurahan').select2({
				placeholder: '--- Pilih Kelurahan ---',
				allowClear: true
			})
			.on('change', function() {
				getRW($(this).val());
			}).trigger('change');;

		$('#RW').select2({
				placeholder: '--- Pilih RW ---',
				allowClear: true
			})
			.on('change', function() {
				getRT($(this).val());
			});

		$('#RT').select2({
			placeholder: '--- Pilih RT ---',
			allowClear: true
		})

		$('#Pendata').select2({
			// placeholder: '--- Pilih Pendata ---',
			// allowClear: true
		})

		$('#JenisData').select2({
				placeholder: '--- Pilih Jenis Data ---',
				minimumResultsForSearch: -1,
			})
			.on('change', function() {
				if ($(this).val() == 1) {
					$('.div-wilayah').show();
					$('.div-pendata').hide();
				} else {
					$('.div-wilayah').hide();
					$('.div-pendata').show();
				}
			}).trigger('change');

		$('#PeriodeSensus').select2({
			placeholder: '--- Pilih Periode Pendataan ---',
			minimumResultsForSearch: -1,
			// allowClear: true
		})

	}

	var getRW = function(id_kelurahan) {
		if (id_kelurahan == '') {
			var el = $('#RW');
			el.children().remove();
			el.append($("<option></option>").attr("value", '').text('--- Select Item ---'));
			el.trigger('change');
			return false;
		}

		KTApp.blockPage({
			message: 'Harap tunggu...'
		});

		var xhr = $.ajax({
				url: base_url + '/wilayah/rw/' + id_kelurahan,
				method: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				var el = $('#RW');
				el.children().remove();
				el.append($("<option></option>").attr("value", '').text('--- Select Item ---'));
				$.each(response, function(key, value) {
					el.append($("<option></option>").attr("value", value.id).text(value.text));
				});
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				toastr.error('getRW: ' + jqXHR.statusText);
			})
			.always(function() {
				KTApp.unblockPage();
			});
	}

	var getRT = function(id_rw) {
		if (id_rw == '') {
			var el = $('#RT');
			el.children().remove();
			el.append($("<option></option>").attr("value", '').text('--- Select Item ---'));
			el.trigger('change');
			return false;
		}

		KTApp.blockPage({
			message: 'Harap tunggu...'
		});

		var xhr = $.ajax({
				url: base_url + '/wilayah/rt/' + id_rw,
				method: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				var el = $('#RT');
				el.children().remove();
				el.append($("<option></option>").attr("value", '').text('--- Select Item ---'));
				$.each(response, function(key, value) {
					el.append($("<option></option>").attr("value", value.id).text(value.text));
				});
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				toastr.error('getRT: ' + jqXHR.statusText);
			})
			.always(function() {
				KTApp.unblockPage();
			});
	}

	var grid;

	var initPivot = function(param) {
		$('#tablemon').jqGrid("GridUnload");
		var indikatorY = ['Pendidikan', 'Perkawinan', 'Pekerjaan'];
		var dimensionYCol = indikatorY[parseInt(param.Indikator) - 1];
		var querystr = $.param(param);

		KTApp.blockPage({
			message: 'Harap tunggu!...'
		});


		if (param.Indikator == 5) {
			var ids;

			grid = $('#tablemon').jqGrid(
				'jqPivot',
				base_url + '/laporan/anomalisensus/data?' + querystr, {
					frozenStaticCols: true,
					skipSortByX: true,
					useColSpanStyle: true,
					xDimension: [
						{ dataName: 'nama_provinsi', label: 'Provinsi'},
						{ dataName: 'nama_kabupaten', label: 'Kabupaten', align: "center", skipGrouping:true},
						{ dataName: 'nama_kecamatan', label: 'Kecamatan', align: "center", skipGrouping:true},
						{ dataName: 'nama_kelurahan', label: 'Kelurahan', align: "center", skipGrouping:true},
						{ dataName: 'nama_rw', label: 'RW', align: "center", skipGrouping:true },
						{ dataName: 'nama_rt', label: 'RT'},					
					],
					yDimension: [{
							dataName: 'indikator'
						}, // group anomali
						{
							dataName: 'indikator2'
						} // group umur
					],
					aggregates: [{
						member: 'qty',
						aggregator: 'sum',
						label: 'Sum',
						formatter: 'integer',
						align: 'right',
						width: 100,
						//formatter: nullFormatter,
						formatter: function(cellvalue, options, rowObject) {

							if (cellvalue > 0) {
								ids = cellvalue;
								return '<span style="background-color: #00cec9; display: block; width: 100%; height: 100%; padding:5px;">' + cellvalue + '</span>';
							} else {
								if (cellvalue == null || cellvalue == 'null') {
									return 0;
								} else {
									return cellvalue;
								}
							}
						},
					}],

				}, {
					pager: '#pagermon',
					shrinkToFit: false,
					forceFit: true,
					//sortable: true,
					viewrecords: true,
					rownumbers: true,
					autowidth: true,
					threeStateSort: true,
					// rowNum: 20,
					rowList: [20, 30, 50],
					loadComplete: function() {
						KTApp.unblockPage();
					},
					onCellSelect: function(rowid, iCol, val, e) {

						var rowData = $(this).jqGrid("getRowData", rowid);
						var row = $(this).jqGrid('getLocalRow', rowid);
						var cm = $(this).jqGrid("getGridParam", "colModel");
						var colName = cm[iCol];

						_Provinsi = row.pivotInfos[colName.name].rows[0].id_provinsi;
						_Kabupaten = row.pivotInfos[colName.name].rows[0].id_kabupaten;
						_Kecamatan = row.pivotInfos[colName.name].rows[0].id_kecamatan;
						_Kelurahan = row.pivotInfos[colName.name].rows[0].id_kelurahan;
						_rW = row.pivotInfos[colName.name].rows[0].id_rw;
						_rT = row.pivotInfos[colName.name].rows[0].id_rt;
						q = row.pivotInfos[colName.name].rows[0].qty;
						a = row.pivotInfos[colName.name].rows[0].Code;

						var param = {
							Indikator: $('#Indikator').val(),
							PeriodeSensus: $('#PeriodeSensus').val(),
							JenisData: $('#JenisData').val(),
							Kelurahan: $('#Kelurahan').val(),
							RW: _rW,
							RT: _rT,
							Provinsi: _Provinsi,
							Kabupaten: _Kabupaten,
							Kecamatan: _Kecamatan,
							Kelurahan: _Kelurahan,
							Pendata: $('#Pendata').val(),
							Code: a,
							umur: b,
							qty: q,
						};

						globalparam = param;

						if (val > 0 || parseInt(q) > 0 || ids > 0) {
							showDetail(param);
						} else {
							message = 'Data Pilihan 0 atau tidak ada detail!.'
							swal.fire('', message, 'error');
						}

					},
				}
			);

		} else {
			var ids;
			$('#tablemon').jqGrid("GridUnload");
			grid = $('#tablemon').jqGrid(
				'jqPivot',
				base_url + '/laporan/anomalisensus/data?' + querystr, {
					frozenStaticCols: true,
					skipSortByX: true,
					useColSpanStyle: true,
					xDimension: [
						{ dataName: 'nama_provinsi', label: 'Provinsi'},
						{ dataName: 'nama_kabupaten', label: 'Kabupaten', align: "center", skipGrouping:true},
						{ dataName: 'nama_kecamatan', label: 'Kecamatan', align: "center", skipGrouping:true},
						{ dataName: 'nama_kelurahan', label: 'Kelurahan', align: "center", skipGrouping:true},
						{ dataName: 'nama_rw', label: 'RW', align: "center", skipGrouping:true },
						{ dataName: 'nama_rt', label: 'RT'},
					],
					yDimension: [
						{ dataName: 'indikators', sortorder: "asc" }, // group anomali
						{ dataName: 'umurx', sortorder: "asc", sorttype: "integer"} // group umur
					],
					aggregates: [{
						member: 'qty',
						aggregator: 'sum',
						label: 'Sum',
						formatter: 'integer',
						align: 'right',
						width: 100,
						formatter: function(cellvalue, options, rowObject) {
							if (cellvalue > 0) {
								ids = cellvalue;
								return '<span style="background-color: #00cec9; display: block; width: 100%; height: 100%; padding:5px;">' + cellvalue + '</span>';
							} else {
								return cellvalue;
							}
						},
					}, ],
				}, {
					pager: '#pagermon',
					shrinkToFit: false,
					forceFit: true,
					sortable: true,
					viewrecords: true,
					rownumbers: true,
					autowidth: true,
					// rowNum: 20,
					rowList: [20, 30, 50],
					loadComplete: function() {
						KTApp.unblockPage();
					},
					onCellSelect: function(rowid, iCol, val, e) {

						var rowData = $(this).jqGrid("getRowData", rowid);
						var row = $(this).jqGrid('getLocalRow', rowid);
						var cm = $(this).jqGrid("getGridParam", "colModel");
						var colName = cm[iCol];

						_Provinsi = row.pivotInfos[colName.name].rows[0].id_provinsi;
						_Kabupaten = row.pivotInfos[colName.name].rows[0].id_kabupaten;
						_Kecamatan = row.pivotInfos[colName.name].rows[0].id_kecamatan;
						_Kelurahan = row.pivotInfos[colName.name].rows[0].id_kelurahan;
						_rW = row.pivotInfos[colName.name].rows[0].id_rw;
						_rT = row.pivotInfos[colName.name].rows[0].id_rt;

						a = row.pivotInfos[colName.name].rows[0].Code;
						b = row.pivotInfos[colName.name].rows[0].umurx;
						q = row.pivotInfos[colName.name].rows[0].qty;

						var param = {
							Indikator: $('#Indikator').val(),
							PeriodeSensus: $('#PeriodeSensus').val(),
							JenisData: $('#JenisData').val(),
							Kelurahan: $('#Kelurahan').val(),
							RW: _rW,
							RT: _rT,
							Provinsi: _Provinsi,
							Kabupaten: _Kabupaten,
							Kecamatan: _Kecamatan,
							Kelurahan: _Kelurahan,
							Pendata: $('#Pendata').val(),
							Code: a,
							umur: b,
							qty: q,
						};

						globalparam = param;

						if (val > 0 || parseInt(q) > 0 || ids > 0) {
							showDetail(param);
						} else {
							message = 'Data Pilihan 0 atau tidak ada detail!.'
							swal.fire('', message, 'error');
						}
					}
				}
			);
		} // end else End If

		window.onerror = function(message, source, lineno, colno, error) {
			alert('Data Tidak Ditemukan');
			KTApp.unblockPage();

		}

	}

	var showReport = function() {
		$('#btnShow').click(function(e) {
			e.preventDefault();

			if ($('#PeriodeSensus').val() == '') {
				swal.fire('', 'Periode Pendataan belum dipilih', 'error');
				return false;
			}

			if ($('#JenisData').val() == '') {
				swal.fire('', 'Jenis Data belum dipilih', 'error');
				return false;
			}

			var param = {
				Indikator: $('#Indikator').val(),
				PeriodeSensus: $('#PeriodeSensus').val(),
				JenisData: $('#JenisData').val(),
				Kelurahan: $('#Kelurahan').val(),
				RW: $('#RW').val(),
				RT: $('#RT').val(),
				Pendata: $('#Pendata').val(),
			};
			initPivot(param);
		});
	}

	var showDetail = function(param) {
		globalparam = param;
		$('#modalDetail2').modal();
	}



	var modalShowCallback = function() {
		$('#modalDetail2').on('shown.bs.modal', function(e) {
			initTable(globalparam);
		});
	}

	var initTable = function(param) {
		var indikatorY = ['Pendidikan', 'Perkawinan', 'Pekerjaan'];
		var dimensionYCol = indikatorY[parseInt(param.Indikator) - 1];
		var querystr = $.param(param);

		$("#tableroles").jqGrid("GridUnload");

		$('#tableroles').jqGrid({
			datatype: 'json',
			mtype: 'GET',
			url: base_url + '/laporan/anomalisensus/datapaging?' + querystr,
			pager: '#pagerroles',
			//postData: param,
			// pager: '#pagerview',
			shrinkToFit: false,
			forceFit: true,
			// sortable: true,
			headertitles: true,
			// viewrecords: true,
			// rownumbers: true,
			autowidth: true,
			rowNum: 100,
			colModel: [{
					label: 'id_frm',
					name: 'id_frm',
				}, {
					name: 'nik',
					label: 'NIK',
					//sortable: 'asc',
				}, {
					name: 'nama_anggotakel',
					label: 'Nama'
				}, {
					name: 'no_urutkel',
					label: 'No Urut Kel.'
				}, {
					name: 'nama_provinsi',
					label: 'Provinsi',
				}, {
					name: 'nama_kabupaten',
					label: 'Kabupaten',
				}, {
					name: 'nama_kelurahan',
					label: 'Kelurahan',
				}, {
					name: 'nama_rw',
					label: 'RW',
				}, {
					name: 'nama_rt',
					label: 'RT',
				}, {
					label: 'Status',
					align: 'center',
					width: 90,
					formatter: function(val, opt, row) {
						var tittle;
						var btnClass;
						if (row.status_sensus == '1') {
							tittle = 'Valid';
							btnClass = 'kt-badge--success';
						} else if (row.status_sensus == '2') {
							tittle = "NotValid";
							btnClass = 'kt-badge--danger';
						} else if (row.status_sensus == '3') {
							tittle = "Anomali";
							btnClass = 'kt-badge--primary';
						} else if (row.status_sensus == '4') {
							tittle = "Anulir";
							btnClass = 'kt-badge--default';
						} else {
							tittle = "Received";
							btnClass = 'kt-badge--warning';
						}

						var span = '<span class="kt-badge ' + btnClass + ' kt-badge--inline">' + tittle + '</span>';
						return span;
					}
				},
				{
					label: 'Action',
					align: 'center',
					formatter: function(val, opt, row) {
						var disabled = row.status_sensus == '2' ? ' btn btn-outline-warning btn-elevate btn-icon btn-sm btnValid" data-id="' + row.id_frm + ' ' : ' btn btn-outline-warning btn-elevate btn-icon btn-sm disabled ';
						var valid = row.status_sensus == '2' ? ' Validate data : ' + row.id_frm + ' ' : ' Record cannot be validate.! ';
						var btnEdit = '<button type="button" title="View Data &quot;' + row.id_frm + '&quot;" class="btn btn-outline-success btn-elevate btn-icon btn-sm btnView" data-id_frm="' + row.id_frm + '"><i class="fa fa-file-alt "></i></button>&nbsp;';
						var btnReset = '<button type="button" title="' + valid + '" class="' + disabled + '"><i class="fa fa-envelope-open-text"></i></button>&nbsp;';
						return btnEdit + btnReset;
					}
				}
			],
		});
	}

	var btnValidHandler = function() {
		$(document).on('click', '.btnValid', function() {
			var selRowId = $(this).data('id');
			var row = $('#tableroles').jqGrid('getRowData', selRowId);

			Swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, Validate it!'
			}).then((result) => {
				if (result.value) {
					KTApp.block('#modalDetail2', {
						message: 'Harap tunggu...'
					});
					$.ajax({
						method: 'GET', // Type of response and matches what we said in the route
						url: base_url + '/laporan/validme/' + selRowId, // This is the url we gave in the route
						success: function(response) { // What to do if we succeed
							if (response.status) {
								Swal.fire(
									'Validate!',
									response.message,
									'success'
								)
								$('#modalDetail2').modal('hide');
							}
						},
						error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
							console.log(JSON.stringify(jqXHR));
							console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
						}
					}).always(function() {
						KTApp.unblock('#modalDetail2');
					});
				} // end result
			}) // end then
		});
	}

	var printPDF = function() {
		$('#btnPrint').click(function(e) {
			e.preventDefault();
			if ($('#PeriodeSensus').val() == '') {
				swal.fire('', 'Periode Pendataan belum dipilih', 'error');
				return false;
			}

			var param = {
				Indikator: $('#Indikator').val(),
				PeriodeSensus: $('#PeriodeSensus').val(),
				JenisData: $('#JenisData').val(),
				Kelurahan: $('#Kelurahan').val(),
				Pendata: $('#Pendata').val(),
				RW: $('#RW').val(),
				RT: $('#RT').val(),
				print: 1,
			};
			var querystr = $.param(param);
			var url = base_url + '/laporan/cetak?' + querystr;

			$('<a href="' + url + '" target="_blank">&nbsp;</a>')[0].click();

		});
	}

	return {
		init: function() {
			select2Handler();
			showReport();
			modalShowCallback();
			btnValidHandler();
			printPDF();
		}
	};
}();


jQuery(document).ready(function() {
	AnomaliSensus.init();
});
