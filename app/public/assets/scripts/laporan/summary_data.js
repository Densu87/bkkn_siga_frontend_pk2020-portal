// "use strict";

var Summary = function() {
    
    var grid;
    
	var select2Handler = function() {

		$('#Wilayah').select2({
			placeholder: '--- Pilih Pengelompokan Wilayah ---',
            minimumResultsForSearch: -1,
		});

		$('#PeriodeSensus').select2({
			placeholder: '--- Pilih Periode Pendataan ---',
            minimumResultsForSearch: -1,
		})

	}
    
    var initTable = function (param) {
        $('#tablemon').jqGrid("GridUnload");
        var param = {
            PeriodeSensus: $('#PeriodeSensus').val(),
            groupby: $('#Wilayah').val(),
        };
        var querystr = $.param(param);
        var xparam = [];
        switch ($('#Wilayah').val()) {
            case '4':
                xparam = [{
                        dataName: 'nama_kelurahan',
                        label: 'Kelurahan'
                    },
                ];
                break;
            case '5':
                xparam = [{
                        dataName: 'nama_kelurahan',
                        label: 'Kelurahan'
                    },{
                        dataName: 'nama_rw',
                        label: 'RW'
                    },
                ];
                break;
            case '6': 
                xparam = [{
                        dataName: 'nama_kelurahan',
                        label: 'Kelurahan'
                    },{
                        dataName: 'nama_rw',
                        label: 'Rw',
                        hidden: true,
                    },{
                        dataName: 'nama_rt',
                        label: 'RT'
                    },
                ];
                break;
        }
        grid = $('#tablemon').jqGrid('jqPivot', base_url + '/laporan/summary/data?' + querystr, 
            {
                frozenStaticCols: true,
                skipSortByX: true,
                useColSpanStyle: true,
                xDimension: xparam,
                yDimension: [{dataName: 'status_nama'}],
                aggregates: [{
                    member: 'qty',
                    aggregator: 'sum',
                    label: 'Sum',
                    formatter: 'integer',
                    align: 'right',
                    // width: 100,
                }],

				}, {
					pager: '#pagermon',
					// shrinkToFit: false,
					// forceFit: true,
					//sortable: true,
					viewrecords: true,
					rownumbers: true,
					autowidth: true,
					threeStateSort: true,
					// rowNum: 20,
					rowList: [20, 30, 50],
					loadComplete: function() {
						KTApp.unblockPage();
					},
				}
			);

    }
    
    var showReport = function () {
        $('#btnShow').click(function (e) {
            e.preventDefault();
            if ($('#PeriodeSensus').val()=='') {
                swal.fire('', 'Periode Pendataan belum dipilih', 'error');
                return false;
            }
            if ($('#Wilayah').val()=='') {
                swal.fire('', 'Pengelompokan Wilayah belum dipilih', 'error');
                return false;
            }
            initTable();
        });
    }
    
    var btnPrintHandler = function () {
        $('#btnPrint').click(function (e) {
            e.preventDefault();
            if ($('#PeriodeSensus').val()=='') {
                swal.fire('', 'Periode Pendataan belum dipilih', 'error');
                return false;
            }
            if ($('#Wilayah').val()=='') {
                swal.fire('', 'Pengelompokan Wilayah belum dipilih', 'error');
                return false;
            }
            
            var param = {
                PeriodeSensus: $('#PeriodeSensus').val(),
                groupby: $('#Wilayah').val(),
                print: 1,
            };
            
            var querystr = $.param( param );
            var url = base_url + '/laporan/summary/data?' + querystr;
            $('<a href="'+url+'" target="_blank">&nbsp;</a>')[0].click();

        });
    }
    
	return {
		init: function() {
            select2Handler();
            showReport();
            btnPrintHandler();
		}
	};
}();

jQuery(document).ready(function() {
	Summary.init();
});