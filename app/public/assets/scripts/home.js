"use strict";

var KTDashboard = function() {
    
    var initTable = function () {
        $('#tabledata').jqGrid({
            datatype: 'json',
            url: base_url+'/home/datasensus',
            // pager: '#pagerroles',
			shrinkToFit: pShrinkToFit,
			forceFit: pForceFit,
            sortable: true,
            // viewrecords: true,
            rownumbers: true,
            autowidth: true,
            rowNum: 10,
            rowList: [5, 10, 20],
            colModel: [
                    {
						label: 'Alamat KK',
						name: 'alamat',
					}, { 
						label: 'Wilayah', 
						name: 'wilayah',
					}, { 
						label: 'Jml Jiwa', 
						name: 'jml_jiwa',
					}, { 
						label: 'Tanggal Dibuat', 
						name: 'create_date',
					}, { 
						label: 'Status', 
						name: 'status_sensus',
					},
            ],
        });

    }
    
    var dailySumChart = function() {
        
        var xhr = $.ajax({
            url: base_url + '/home/dailysumdata',
            method: 'GET',
            dataType: 'json',
        })
        .done(function(response) {
            
            var chartContainer = KTUtil.getByID('dailysumchart');
            if (!chartContainer) {
                return;
            }
            
            var chart = new Chart(chartContainer, {
                type: 'bar',
                data: response,
                options: {
                    responsive: true,
                    legend: {
                        display: false,
                    },
                    title: {
                        display: false,
                        text: 'Chart.js Bar Chart'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log('jqXHR', jqXHR);
            toastr.error('dailySumChart: ' + jqXHR.statusText);
        })
        .always(function() {});

		
	}
    
    var statusSensusChart = function() {
        
        var xhr = $.ajax({
            url: base_url + '/home/statussensus',
            method: 'GET',
            dataType: 'json',
        })
        .done(function(response) {
            console.log(response);
            if (typeof response.customdata.totaldata !== 'undefined') {
                $('#totaldatasensus').html(response.customdata.totaldata);
            }
            var chartContainer = KTUtil.getByID('statussensuschart');
            if (!chartContainer) {
                return;
            }
            
            var chart = new Chart(chartContainer, {
                type: 'doughnut',
                data: response,
                options: {
                    responsive: true,
                    legend: {
                        position: 'right',
                    },
                    title: {
                        display: false
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log('jqXHR', jqXHR);
            toastr.error('statusSensusChart: ' + jqXHR.statusText);
        })
        .always(function() {});
            
		var chartContainer = KTUtil.getByID('dailysumchart');

		
	}
    
    var statPendata = function() {
        
        var xhr = $.ajax({
            url: base_url + '/home/statpendata',
            method: 'GET',
            dataType: 'json',
        })
        .done(function(response) {
            if(response) {
                console.log(response);
                $('#totalpendata').html(response.total);
                response.data.forEach(el => {
                    if (el.TingkatWilayahID===null) {
                        $('#pendatatw0').html(el.total);
                    } else {
                        $('#pendatatw'+el.TingkatWilayahID).html(el.total);
                    }
                });
            }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log('jqXHR', jqXHR);
            toastr.error('dailySumChart: ' + jqXHR.statusText);
        })
        .always(function() {});

		
	}
    
	return {
		init: function() {
            initTable();
            dailySumChart();
            statusSensusChart();
            statPendata();
		}
	};
}();

jQuery(document).ready(function() {
	KTDashboard.init();
});