<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="../../">
		<meta charset="utf-8" />
		<title>
            @hasSection('title') @yield('title') |
            @endif
            BKKBN PK2020
        </title>
		<meta name="description" content="Page with empty content">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <meta name="base_url" content="{{ url("/") }}">
		<!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">-->
		<link href="{{ url('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{ url('/favicon.ico') }}">

        <script>
            var base_url = '{{ url("/") }}';
            var csrf_token = '{{ csrf_token() }}';
        </script>
	</head>
	<body class="kt-page--loading-enabled kt-page--loading kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">

		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="{{ url('/') }}">
					<img alt="Logo" src="{{ url('assets/media/logos/logo-2-sm.png') }}" />
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
			</div>
		</div>

		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
						@include('layouts.app_header')
						@include('layouts.app_navigation')

					</div>

                    <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
								<div class="kt-container ">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">&nbsp;</h3>
									</div>
                                </div>
							</div>

                            @hasSection('content')
                                @include('layouts.app_content')
                            @endif

                            @hasSection('content_home')
                                @include('layouts.app_content_home')
                            @endif

						</div>
					</div>
                    
					<div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer">
						@include('layouts.app_footer')
					</div>
                    
				</div>
			</div>
		</div>
        
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
        
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#374afb",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<script src="{{ url('assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/custom.js') }}" type="text/javascript"></script>
        @yield('script')
	</body>
    
</html>
