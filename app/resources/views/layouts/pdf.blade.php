<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="../../">
		<meta charset="utf-8" />
		<title>
            @hasSection('title') @yield('title') |
            @endif
            PK BKKBN
        </title>
		<meta name="description" content="Page with empty content">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link href="{{ url('assets/css/paper.css') }}" rel="stylesheet" type="text/css" />
        <style>@page { size: @yield('pageformat')  }</style>
	</head>
    <body class="@yield('pageformat')">

    <main role="main" class="container sheetx">

        @yield('content')
    </main>

</body>
</html>
