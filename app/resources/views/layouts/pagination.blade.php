<div class="kt-pagination kt-pagination--brand kt-pagination--circle">
    @if ($paginator->hasPages())
    <ul class="kt-pagination__links">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <!--<li class="kt-pagination__link--first">
                <a href="#"><i class="fa fa-minus kt-font-brand"></i></a>
            </li>-->
          </br>
        @else
        <li class="kt-pagination__link--prev">
            <a href="{{ $paginator->previousPageUrl() }}"><i class="fa fa-angle-left kt-font-brand"></i></a>
            <!--<a href="{{ $paginator->previousPageUrl() }}" class="page-link" rel="prev">&laquo;</a></li>-->
            @endif {{-- Pagination Elements --}}
            @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled">{{ $element }}</li>
            @endif {{-- Array Of Links --}}
            @if (is_array($element))
            @foreach ($element as $page => $url)
            @if ($page == $paginator->currentPage())
                <li class="kt-pagination__link--active">
                    <a href="#">{{ $page }}<span class="sr-only">(current)</span></a>
                </li>
            @else
                <li>
                    <a href="{{ $url }}">{{ $page }}</a>
                </li>
            @endif
            @endforeach
            @endif
            @endforeach {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="kt-pagination__link--prev">
                    <!--<a href="{{ $paginator->nextPageUrl() }}" class="page-link" rel="next">&raquo;</a>-->
                    <a href="{{ $paginator->nextPageUrl() }}"><i class="fa fa-angle-right kt-font-brand"></i></a>
                </li>
            @else
                <!--<li class="page-item disabled">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>-->
                </br>
            @endif
    </ul>
    @endif

    <div class="kt-pagination__toolbar">
      <form>
        <select id="pagination" class="form-control kt-font-brand" style="width: 60px">
            <option value="5" @if( $items == 5) selected @endif >5</option>
            <option value="10" @if( $items == 10) selected @endif >10</option>
            <option value="20" @if( $items  == 30) selected @endif >20</option>
            <option value="30" @if( $items  == 30) selected @endif >30</option>
            <option value="50" @if( $items  == 50) selected @endif >50</option>
            <option value="100" @if( $items  == 100) selected @endif >100</option>
        </select>
    </form>
        <span class="pagination__desc">
         Displaying {{ $paginator->perPage() }}  of {{ $paginator->total() }}  records
        </span>
    </div>
</div>

<style>
    .kt-pagination__link--first {
        pointer-events: none; //This makes it not clickable
        opacity: 0.6; //This grays it out to look disabled
    }
</style>

<script>
        document.getElementById('pagination').onchange = function() {
            window.location = "{{ $members->url(1) }}&items=" + this.value;
        };
    </script>
