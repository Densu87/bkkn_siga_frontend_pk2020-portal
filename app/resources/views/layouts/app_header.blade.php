<div class="kt-header__top">
							<div class="kt-container ">

								<!-- begin:: Brand -->
								<div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
									<div class="kt-header__brand-logo">
										<a href="{{ url('/') }}">
											<img alt="Logo" src="{{ url('assets/media/logos/bkkbn-logo.png') }}" class="kt-header__brand-logo-default" height="90" />
											<img alt="Logo" src="{{ url('assets/media/logos/logo-2-sm.png') }}" class="kt-header__brand-logo-sticky" />
										</a>
									</div>
								</div>

								<!-- end:: Brand -->

								<!-- begin:: Header Topbar -->
								<div class="kt-header__topbar">
@guest

<div class="kt-header__topbar-item kt-header__topbar-item--user">
										<div class="kt-header__topbar-wrapper">
                                        <a href="{{ route('login') }}" class="btn btn-secondary"><i class="fas fa-sign-in-alt"></i> Signin</a>
										</div>
									</div>
                                    @endguest
                                    @auth
									<!--begin: User bar -->
									<div class="kt-header__topbar-item kt-header__topbar-item--user">
										<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px" aria-expanded="false">
											<span class="kt-header__topbar-username">{{ currentUser('NamaLengkap') }}</span>
											<img class="kt-hidden-" alt="Pic" src="{{ url('assets/media/users/default.jpg') }}">

										</div>
										<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">


											<!--begin: Navigation -->
											<div class="kt-notification">
												<a href="{{ url('user/profile') }}" class="kt-notification__item">
													<div class="kt-notification__item-icon">
														<i class="flaticon2-calendar-3 kt-font-success"></i>
													</div>
													<div class="kt-notification__item-details">
														<div class="kt-notification__item-title kt-font-bold">
															My Profile
														</div>
														<div class="kt-notification__item-time">
															Account settings and more
														</div>
													</div>
												</a>
                                                <div class="kt-notification__custom kt-space-between">
													<a href="{{ url('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
												</div>
											</div>

											<!--end: Navigation -->
										</div>
									</div>

									<!--end: User bar -->
@endauth
								</div>

								<!-- end:: Header Topbar -->
							</div>
						</div>
                        