@extends('layouts.app')

@section('title', 'Daftar User')

@section('content')
<div class="row kt-margin-b-25">
    <div class="col-sm-9 kt-margin-b-20-tablet-and-mobile">
        <button type="button" class="btn btn-primary" id="btnCreate"><i class="fa fa-user-plus"></i>Tambah User</button>
        @if(currentUser('RoleID')<3)
        <button type="button" class="btn btn-primary" id="btnImport"><i class="fa fa-users"></i>Generate User Kecamatan</button>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table id="tableroles"></table>
        <div id="pagerroles"></div>
    </div>
</div>

<!--begin::modalRoleCreate-->
<div class="modal fade" id="modalRoleCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i></i>Tambah User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="formUserCreate">
                    <div class="form-group">
                        <label for="name" class="form-control-label">Username</label>
                        <!--<input type="text" class="form-control" name="UserName" id="UserName">-->
                        <div class="input-group">
                          <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="la la-group"></i></span></div>
            							<input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon2" name="UserName" id="UserName">
            						</div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="form-control-label">Nama Lengkap</label>
                        <!--<input type="text" class="form-control" name="NamaLengkap" id="NamaLengkap">-->
                        <div class="input-group">
                          <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="la la-black-tie"></i></span></div>
            							<input type="text" class="form-control" placeholder="Nama Lengkap User" aria-describedby="basic-addon2" name="NamaLengkap" id="NamaLengkap">
            						</div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="form-control-label">Email</label>
                        <!--<input type="text" class="form-control" name="Email" id="Email">-->
                        <div class="input-group">
							          <div class="input-group-prepend"><span class="input-group-text">@</span></div>
							                 <input type="email" class="form-control" placeholder="Email" aria-describedby="basic-addon1" name="Email" id="Email">
						            </div>
                    </div>
                    <div class="form-group kt-hidden">
                        <label for="name" class="form-control-label">Password</label>
                        <div class="input-group">
                          <div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="la la-key"></i></span></div>
            							<input type="password" class="form-control" placeholder="Input Password" aria-describedby="basic-addon2" name="Password" id="Password" value="{{ $parameter['Pwd'] ?? '' }} ">
            						</div>
                    </div>
                    <div class="form-group">
                        <label for="RoleID">Pilih Role</label>
                              <select class="form-control" id="RoleID" name="RoleID">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="TingkatWilayahID">Pilih Wilayah</label>
                              <select class="form-control" id="TingkatWilayahID" name="TingkatWilayahID">
                        </select>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-primary" id="btnSave">Add User Login</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--end::modalRoleCreate-->

<!--begin::modalResetCreate-->
<div class="modal fade" id="modalResetCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i></i>Reset Password User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="formPasswordReset">
                    <input type="hidden" name="ID" id="ID">
                    <div class="form-group">
                        <label for="name" class="form-control-label">New Password</label><label style="color:red;">&nbsp; *</label>
                        <input type="password" class="form-control" name="Password" id="Password">
                    </div>
                    <div class="form-group">
                        <label for="name" class="form-control-label">Confirmation Password</label><label style="color:red;">&nbsp; *</label>
                        <input type="password" class="form-control" name="rePassword" id="rePassword">
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-warning" id="btnResetSave">Reset Password</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--end::modalResetCreate-->

<!--begin::modalUserEdit-->
<div class="modal fade" id="modalUserEdit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="formUserEdit">
                    <input type="hidden" name="ID" id="ID">
                    <div class="form-group">
                        <label for="RoleName" class="form-control-label">Username</label>
                        <input type="text" class="form-control" name="UserName" id="UserName" disabled>
                    </div>
                    <div class="form-group">
                        <label for="RoleName" class="form-control-label">Email</label>
                        <input type="text" class="form-control" name="Email" id="Email" disabled>
                    </div>
                    <div class="form-group">
                        <label for="RoleName" class="form-control-label">Nama Lengkap</label>
                        <input type="text" class="form-control" name="NamaLengkap" id="NamaLengkap">
                    </div>
                    <div class="form-group">
                        <label for="RoleName" class="form-control-label">Alamat</label>
                        <input type="text" class="form-control" name="Alamat" id="Alamat">
                    </div>
                    <div class="form-group">
                        <label for="Level" class="form-control-label">No Telp</label>
                        <input type="number" min="1" class="form-control" name="NoTelepon" id="NoTelepon">
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-primary" id="btnUpdateUser">Update User Info</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--end::modalUserEdit-->

@include('user.wilayah')

@endsection

@section('script')
<link href="{{ url('assets/plugins/jqGrid/themes/base/theme.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/plugins/jqGrid/css/ui.jqgrid.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/plugins/jqGrid/css/ui.jqgrid.custom.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ url('assets/plugins/jqGrid/jquery.jqgrid.min.js') }}"></script>
<script src="{{ url('assets/plugins/jqGrid/jquery.jqgrid.custom.js') }}"></script>
<script src="{{ url('assets/scripts/user/user.index.js') }}"></script>
<script src="{{ url('assets/scripts/user/user.wilayah.js') }}"></script>
@endsection
