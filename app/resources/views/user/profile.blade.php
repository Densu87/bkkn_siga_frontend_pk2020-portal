@extends('layouts.app')

@section('title', 'Profil Pengguna')

@section('content')
<div class="form-group row">
    <label class="col-md-3 col-form-label">Username</label>
    <div class="col-md-9">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
            <input class="form-control" type="text" value="{{ currentUser('UserName') }}" readonly>
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label">Nama Lengkap</label>
    <div class="col-md-9">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-black-tie"></i></span></div>
            <input class="form-control" type="text" value="{{ currentUser('NamaLengkap') }}" readonly>
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label">Email</label>
    <div class="col-md-9">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text">@</span></div>
            <input class="form-control" type="text" value="{{ currentUser('Email') }}" readonly>
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label">No. Telepon</label>
    <div class="col-md-9">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
            <input class="form-control" type="text" value="{{ currentUser('NoTelepon') }}" readonly>
        </div>
    </div>
</div>

<div class="form-group row kt-hidden">
    <div class="col-md-9 offset-md-3">
        <button type="button" class="btn btn-primary">Simpan Profile</button>
    </div>
</div>


<div class="form-group row">
    <label class="col-md-12 col-form-label"><hr /><h5>Perubahan Password</h5></label>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label">Password Lama</label>
    <div class="col-md-9">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-key"></i></span></div>
            <input class="form-control" name="oldpassword" id="oldpassword" type="password" value="">
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label">Password Baru</label>
    <div class="col-md-9">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-key"></i></span></div>
            <input class="form-control" name="newpassword" id="newpassword" type="password" value="">
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label">Konfirmasi Password Baru</label>
    <div class="col-md-9">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-key"></i></span></div>
            <input class="form-control" name="newpasswordconfirm" id="newpasswordconfirm" type="password" value="">
        </div>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-9 offset-md-3">
        <button type="button" class="btn btn-primary" id="btnChangePwd">Ubah Password</button>
    </div>
</div>

@endsection

@section('script')
<script src="{{ url('assets/scripts/user/user.profile.js') }}"></script>
@endsection
