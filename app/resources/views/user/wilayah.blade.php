

<!--begin::modalWilayah-->
<div class="modal fade" id="modalWilayah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Akses Wilayah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form" id="formUserAkses">
                    <input type="hidden" name="MaxTarget" id="MaxTarget" value="{{ $parameter['MaxTarget']?? 0  }}">
                    <input type="hidden" name="Is1Kelurahan" id="Is1Kelurahan" value="{{ $parameter['Is1Kelurahan']?? 0  }}">
                    <input type="hidden" name="UserID" id="UserID" value="">
                    <input type="hidden" name="TingkatWilayahIDUser" id="TingkatWilayahIDUser" value="">
                    
                    <div class="form-group row">
                        <label class="col-sm-3">Tingkat Wilayah {{ currentUser('RoleName') }}</label>
                        <div class="col-sm-3">
                            <p class="form-control-static">: {{ currentUser('TingkatWilayah') }}</p>
                        </div>
                        <label for="TingkatWilayahUser" class="col-sm-3">Tingkat Wilayah User</label>
                        <div class="col-sm-3">
                            <p class="form-control-static">: <span id="TingkatWilayahUser"></span></p>
                        </div>
                    </div>
                    <div class="form-group" id="wilayah_tree_wrapper">
                        <div class="kt-scroll" data-scroll="true" data-height="300">
                            <table id="treewilayah"></table>
                            <div id="pagerwilayah"></div>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-primary" id="btnUpdateAkses">Update Wilayah</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--end::modalWilayah-->