@extends('layouts.app')

@section('title', 'Beranda')

@section('content_home')
<style>

</style>
<div class="row">

    @auth()
    <div class="col-sm-4">
        <div class="kt-portlet kt-portlet--height-fluid kt-callout kt-callout--info">
            <div class="kt-widget14">
                <div class="kt-widget14__header">
                    <h3 class="kt-widget14__title"><span class="kt-badge kt-badge--unified-info kt-badge--lg kt-badge--bold"><span class="fa fa-user"></span></span> {{ currentUser('NamaLengkap') }}</h3>
                    <span class="kt-widget14__desc">{{ currentUser('RoleName') }}</span>
                </div>
                <div class="kt-widget__body">
                    <div class="row kt-margin-b-10">
                        <div class="col-md-4">UserName:</div>
                        <div class="col-md-8">{{ currentUser('UserName') }}</div>
                    </div>
                    <div class="row kt-margin-b-10">
                        <div class="col-md-4">Email:</div>
                        <div class="col-md-8">{{ currentUser('Email') }}</div>
                    </div>
                    <div class="row kt-margin-b-10">
                        <div class="col-md-4">No Telepon:</div>
                        <div class="col-md-8">{{ currentUser('NoTelepon') }}</div>
                    </div>
                    <div class="row kt-margin-b-10">
                        <div class="col-md-4">Tingkat:</div>
                        <div class="col-md-8">{{ currentUser('TingkatWilayah') }}</div>
                    </div>
                    <div class="row kt-margin-b-10">
                        <div class="col-md-4">Wilayah:</div>
                        <div class="col-md-8">
                            <ul>
                            @foreach (auth()->user()->AksesWilayah as $item)
                            <li>{{ $item->nama_wilayah }}</li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endauth()
        
    <div class="col-sm-4">
            
        
        
    
        <div class="kt-portlet kt-portlet--height-fluid kt-callout">
            <div class="kt-widget14">
                <div class="kt-widget14__header">
                    <h3 class="kt-widget14__title"><span class="kt-badge kt-badge--unified-info kt-badge--lg kt-badge--bold"><i class="fas fa-chart-pie"></i></span> STATISTIK STATUS PENDATAAN</h3>
                    <span class="kt-widget14__desc">Total Data: <span id="totaldatasensus">0</span></span>
                </div>
                <div class="kt-widget__body">
                    <canvas id="statussensuschart"></canvas>
                </div>
            </div>
        </div>
    </div>


    <div class="col-sm-4">
        <div class="kt-portlet kt-portlet--height-fluid kt-callout">

            <div class="kt-widget14">
                <div class="kt-widget14__header">
                    <h3 class="kt-widget14__title"><span class="kt-badge kt-badge--unified-info kt-badge--lg kt-badge--bold"><span class="fa fa-users"></span></span> JUMLAH PENDATA</h3>
                    <span class="kt-widget14__desc">{{ auth()->check() ? 'Di Wilayah Anda' : 'Nasional' }}</span>
                </div>
                <div class="kt-widget__body">
                    <div class="row">
                        <div class="col-md-6 kt-widget14__chart" title="Total Pendata"><div class="kt-widget14__stat" id="totalpendata">0</div></div>
                        <div class="col-md-6">
                            <p title="Tingkat Wilayah RW">
                                <span class="kt-badge kt-badge--success kt-badge--lg">RW</span>&nbsp;
                                <span class="text-stat" id="pendatatw5">0</span>
                            </p>
                            <p title="Tingkat Wilayah RT">
                                <span class="kt-badge kt-badge--info kt-badge--lg">RT</span>&nbsp;
                                <span class="text-stat" id="pendatatw6">0</span>
                            </p>
                            <p title="Tingkat Wilayah Tidak Terdefinisi">
                                <span class="kt-badge kt-badge--danger kt-badge--lg">N/A</span>&nbsp;
                                <span class="text-stat" id="pendatatw0">0</span>
                            </p>
                        </div>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row">
    
    <div class="col-md-8">
        <div class="kt-portlet kt-portlet--height-fluid kt-callout">
            <div class="kt-widget14">
                <div class="kt-widget14__header">
                    <h3 class="kt-widget14__title"><span class="kt-badge kt-badge--unified-info kt-badge--lg kt-badge--bold"><span class="fas fa-table"></span></span> DATA TERBARU</h3>
                    <span class="kt-widget14__desc">&nbsp;</span>
                </div>
                <div class="kt-widget__body">
                    <table id="tabledata"></table>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-4">
    
        <div class="kt-portlet kt-portlet--height-fluid kt-callout">
            <div class="kt-widget14">
                <div class="kt-widget14__header">
                    <h3 class="kt-widget14__title"><span class="kt-badge kt-badge--unified-info kt-badge--lg kt-badge--bold"><span class="fa fa-chart-bar"></span></span> JUMLAH DATA HARIAN</h3>
                    <span class="kt-widget14__desc">&nbsp;</span>
                </div>
                <div class="kt-widget__body">
                    <canvas id="dailysumchart"></canvas>
                </div>
            </div>
        </div>
        
        
    </div>
    
</div>
@endsection

@section('script')
<link href="{{ url('assets/plugins/jqGrid/themes/base/theme.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/plugins/jqGrid/css/ui.jqgrid.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/plugins/jqGrid/css/ui.jqgrid.custom.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ url('assets/plugins/jqGrid/jquery.jqgrid.min.js') }}"></script>
<script src="{{ url('assets/plugins/jqGrid/jquery.jqgrid.custom.js') }}"></script>
<script src="{{ url('assets/scripts/home.js') }}"></script>
@endsection
