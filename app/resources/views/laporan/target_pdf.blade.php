@extends('layouts.pdf')

@section('title', 'Monitoring Target vs Aktual')
@section('pageformat', 'A4 landscape')

@section('content')
<div class="row">
    <div class="col-xs-12"><h2>Monitoring Target vs Aktual</div>
</div>
<div class="row mb">
    <div class="col-xs-3">Periode Pendataan</div>
    <div class="col-xs-8">: 2020</div>
</div>
<div class="row mb">
    <div class="col-xs-3">Wilayah</div>
    <div class="col-xs-8">: {{ $nama_wilayah }}</div>
</div>

<div class="row">
    <div class="col-md-12">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Pendata</th>
            <th>Kelurahan</th>
            <th>RW</th>
            <th>RT</th>
            <th>Target</th>
            <th>Aktual</th>
        </tr>
    </thead>
    <tbody>
        @foreach($rows as $row)
        <tr>
            <td>{{ $row->NamaLengkap }}</td>
            <td>{{ $row->nama_kelurahan }}</td>
            <td>{{ $row->nama_rw }}</td>
            <td>{{ $row->nama_rt }}</td>
            <td>{{ $row->TargetKK }}</td>
            <td>{{ $row->Actual }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
@endsection