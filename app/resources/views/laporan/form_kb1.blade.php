@extends(request()->ajax() ? 'layouts.ajax' : 'layouts.app')

@section('title', 'Form KB1')

@section('content')
<div class="row">
   <div class="col-sm-12">
   
       <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refkb[1]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refkb[1]['question_text'] }} <span class="frm-answer">{{ $frmkb[1]['varnum1'] ?? '_' }}</span> Anak</p>
            </div>
        </div>
        
       <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refkb[2]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refkb[2]['question_text'] }} <span class="frm-answer">{{ $frmkb[2]['varnum1'] ?? '_' }}</span></p>
                
                <p>{{ $refanswer[2][1]['id_answer'] ?? '' }}. {{ $refanswer[2][1]['answer_text'] ?? '' }} Laki-laki <span class="frm-answer">{{ $frmkbanswer[2][1]['varnum1'] ?? '_' }}</span> Perempuan <span class="frm-answer">{{ $frmkbanswer[2][1]['varnum2'] ?? '_' }}</span></p>
                <p>{{ $refanswer[2][2]['id_answer'] ?? '' }}. {{ $refanswer[2][2]['answer_text'] ?? '' }} Laki-laki <span class="frm-answer">{{ $frmkbanswer[2][2]['varnum1'] ?? '_' }}</span> Perempuan <span class="frm-answer">{{ $frmkbanswer[2][2]['varnum2'] ?? '_' }}</span></p>
            </div>
        </div>
        
       <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refkb[3]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refkb[3]['question_text'] }}</p>
                
                @if (!empty($frmkbanswer[3][1]))
                    <p>
                        {{ $refanswer[3][1]['answer_text'] }}, Usia Kehamilan <span class="frm-answer" id="frma_2_varnum1">{{ $frmkbanswer[3][1]['varnum1'] ?? '_' }}</span> Minggu
                    </p>
                    <p><strong>Jika ya, saat mulai hamil, apakah Ibu memang ingin hamil saat itu, ingin hamil nanti atau tidak ingin anak lagi ?</strong></p>
                    @if ($frmkbanswer[3][1]['pilihankb']=='1')
                        <p>1. Ya, ingin hamil saat itu</p>
                    @elseif ($frmkbanswer[3][1]['pilihankb']=='2')
                        <p>2. Ingin hamil nanti/kemudian</p>
                    @elseif ($frmkbanswer[3][1]['pilihankb']=='3')
                        <p>3. Tidak ingin anak lagi</p>
                    @endif
                @endif
                
                
                @if (!empty($frmkbanswer[3][2]))
                    <p>
                        {{ $refanswer[3][2]['answer_text'] }}
                    </p>
                    <p><strong>Jika tidak, apakah Ibu menginginkan anak (lagi) ?</strong></p>
                    @if ($frmkbanswer[3][2]['pilihankb']=='1')
                        <p>1. Ya, ingin anak segera</p>
                    @elseif ($frmkbanswer[3][2]['pilihankb']=='2')
                        <p>2. Ya, ingin anak nanti/kemudian</p>
                    @elseif ($frmkbanswer[3][2]['pilihankb']=='3')
                        <p>3. Tidak ingin anak lagi</p>
                    @endif
                @endif
            
            </div>
        </div>
        
       <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refkb[4]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refkb[4]['question_text'] }}</p>
               
                @if (!empty($frmkbanswer[4][1]))
                    <p>{{ $refanswer[4][1]['answer_text'] }}</p>
                @endif
                @if (!empty($frmkbanswer[4][2]))
                    <p>{{ $refanswer[4][2]['answer_text'] }}</p>
                @endif
                @if (empty($frmkbanswer[4]))
                    <p>_</p>
                @endif
            </div>
        </div>
        
       <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refkb[5]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refkb[5]['question_text'] }}</p>
               
                @if (!empty($frmkbanswer[5][1]))
                    <p>{{ $refanswer[5][1]['answer_text'] }}</p>
                    <p>Kapan mulai menggunakan alat/obat Kontrasepsi (KB) terakhir?</p>
                    <p>Bulan  <span class="frm-answer">{{ $frmkbanswer[5][1]['varnum1'] ?? '_' }}</span> Tahun <span class="frm-answer">{{ $frmkbanswer[5][1]['varnum2'] ?? '_' }}</span></p>
                    <p>Kapan Berhenti menggunakan alat/obat Kontrasepsi (KB) terakhir?</p>
                    <p>Bulan  <span class="frm-answer">{{ $frmkbanswer[5][1]['varnum3'] ?? '_' }}</span> Tahun <span class="frm-answer">{{ $frmkbanswer[5][1]['varnum4'] ?? '_' }}</span></p>
                @endif
                @if (!empty($frmkbanswer[5][2]))
                    <p>{{ $refanswer[5][2]['answer_text'] }}</p>
                @endif
                @if (empty($frmkbanswer[5]))
                    <p>_</p>
                @endif
            </div>
        </div>
        
       <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refkb[6]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refkb[6]['question_text'] }}</p>
                
                @foreach ($refanswer[6] as $ref)
                    @if (!empty($frmkbanswer[6][$ref['id_answer']]))
                        <p>- {{ $ref['answer_text'] }}</p>
                    @endif
                @endforeach
            </div>
        </div>
        
       <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refkb[7]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refkb[7]['question_text'] }}</p>
                <p>Bulan  <span class="frm-answer">{{ $frmkb[7]['varnum1'] ?? '_' }}</span> Tahun <span class="frm-answer">{{ $frmkb[7]['varnum2'] ?? '_' }}</span></p>
            </div>
        </div>
        
       <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refkb[8]['id'] }}</div>
            <div class="col-sm-11">
               <p>{{ $refkb[8]['question_text'] }}</p>
               
                @foreach ($refanswer[8] as $ref)
                    @if (!empty($frmkbanswer[8][$ref['id_answer']]))
                        <p>- {{ $ref['answer_text'] }}
                            @if ($ref['id_answer']==10)
                            : {{ $frmkbanswer[8][$ref['id_answer']]['varnum1'] }}
                            @endif
                        </p>
                    @endif
                @endforeach
            </div>
        </div>
       <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refkb[9]['id'] }}</div>
            <div class="col-sm-11">
               <p>{{ $refkb[9]['question_text'] }}</p>
                @foreach ($refanswer[9] as $ref)
                    <p>- {{ $ref['answer_text'] }}
                        <span class="frm-answer">
                        @if (!empty($frmkbanswer[9][$ref['id_answer']]))
                            @if ($frmkbanswer[9][$ref['id_answer']]['pilihankb']==1)
                                YA
                            @elseif ($frmkbanswer[9][$ref['id_answer']]['pilihankb']==2)
                                TIDAK
                            @else
                                _
                            @endif
                        @else 
                            _
                        @endif
                        </span>
                    </p>
                @endforeach
            </div>
        </div>
       <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refkb[10]['id'] }}</div>
            <div class="col-sm-11">
               <p>{{ $refkb[10]['question_text'] }}</p>
               
               @foreach ($refanswer[10] as $ref)
                    @if (!empty($frmkbanswer[10][$ref['id_answer']]))
                        <p>- {{ $ref['answer_text'] }}
                            @if ($ref['id_answer']==13)
                            : {{ $frmkbanswer[10][$ref['id_answer']]['varnum1'] }}
                            @endif
                        </p>
                    @endif
                @endforeach
            </div>
        </div>
   </div>
</div>


@endsection