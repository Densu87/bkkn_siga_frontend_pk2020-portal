@extends('layouts.app')

@section('title', 'Monitoring Summary Data')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <form class="kt-form kt-form--label-left" id="formFilter">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="PeriodeSensus">Periode Pendataan</label>
                <div class="col-sm-2">
                    <select class="form-control form-control" name="PeriodeSensus" id="PeriodeSensus" {{ empty($valperiode) ? '' : 'disabled' }}>
                        <option value=""></option>
                        @foreach ($periode as $item)
                            <option value="{{ $item->Tahun }}" {{ $item->Tahun==$valperiode ? 'selected':'' }}>{{ $item->Tahun }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="Wilayah">Pengelompokan Wilayah</label>
                <div class="col-sm-4">
                    <select class="form-control form-control" name="Wilayah" id="Wilayah">
                        <option value=""></option>
                        @foreach ($tingkatwilayah as $item)
                            <option value="{{ $item->ID }}" >{{ $item->TingkatWilayah }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-3">
                <button type="button" class="btn btn-primary" id="btnShow">Tampilkan</button>
                <button type="button" class="btn btn-primary kt-hiddenx" id="btnPrint">Cetak</button> 
                <span class="kt-badge kt-badge--outline kt-badge--warning kt-badge--md" data-toggle="kt-popover" title="" data-content="Bila pra-tinjau cetakan tidak tampil di browser, harap periksa aplikasi pengunduh atau folder default Unduhan (Download)" data-original-title="">!</span> 
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table id="tablemon"></table>
        <div id="pagermon"></div>
    </div>
</div>

@endsection

@section('script')
<link href="{{ url('assets/plugins/jqGrid/themes/base/theme.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/plugins/jqGrid/css/ui.jqgrid.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/plugins/jqGrid/css/ui.jqgrid.custom.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ url('assets/plugins/jqGrid/jquery.jqgrid.min.js') }}"></script>
<script src="{{ url('assets/plugins/jqGrid/jquery.jqgrid.custom.js') }}"></script>
<script src="{{ url('assets/scripts/laporan/summary_data.js') }}"></script>
@endsection
