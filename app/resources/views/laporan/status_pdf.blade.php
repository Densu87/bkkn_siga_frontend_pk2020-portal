@extends('layouts.pdf')

@section('title', 'Monitoring Status Pendataan')
@section('pageformat', 'A4 landscape')

@section('content')
<div class="row">
    <div class="col-xs-12"><h2>Monitoring Status Pendataan</div>
</div>
<div class="row mb">
    <div class="col-xs-3">Periode Pendataan</div>
    <div class="col-xs-8">: 2020</div>
</div>
<div class="row mb">
    <div class="col-xs-3">Status Pendataan</div>
    <div class="col-xs-8">: {{ $status_text }}</div>
</div>
<div class="row mb">
    <div class="col-xs-3">Wilayah</div>
    <div class="col-xs-8">: {{ $nama_wilayah }}</div>
</div>

<div class="row">
    <div class="col-md-12">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No KK</th>
            <th>Alamat</th>
            <th>Provinsi</th>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Kelurahan</th>
            <th>RW</th>
            <th>RT</th>
            <th>Pendata</th>
        </tr>
    </thead>
    <tbody>
        @foreach($rows as $row)
        <tr>
            <td>{{ $row->no_kk }}</td>
            <td>{{ $row->alamat1 }} <br> No Rumah: {{ $row->no_urutrmh }}</td>
            <td>{{ $row->nama_provinsi }}</td>
            <td>{{ $row->nama_kabupaten }}</td>
            <td>{{ $row->nama_kecamatan }}</td>
            <td>{{ $row->nama_kelurahan }}</td>
            <td>{{ $row->nama_rw }}</td>
            <td>{{ $row->nama_rt }}</td>
            <td>{{ $row->NamaLengkap }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
@endsection