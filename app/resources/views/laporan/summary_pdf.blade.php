@extends('layouts.pdf')

@section('title', 'Monitoring Summary Pendataan')
@section('pageformat', 'A4 landscape')

@section('content')
<div class="row">
    <div class="col-xs-12"><h2>Monitoring Summary Pendataan</div>
</div>
<div class="row mb">
    <div class="col-xs-3">Periode Pendataan</div>
    <div class="col-xs-8">: {{ $periode }}</div>
</div>
<div class="row mb">
    <div class="col-xs-3">Wilayah</div>
    <div class="col-xs-8">: </div>
</div>
<div class="row">
    <div class="col-md-12">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Kelurahan</th>
            @if (in_array(request()->input('groupby'), [5,6]))
            <th>RW</th>
            @endif
            @if (in_array(request()->input('groupby'), [6]))
            <th>RT</th>
            @endif
            <th>Anomali</th>
            <th>Anulir</th>
            <th>NotValid</th>
            <th>Received</th>
            <th>Valid</th>
        </tr>
    </thead>
    <tbody>
        @foreach($rows as $row)
            @php
                $qty = str_replace('\'', '"', $row->qty);
                $qty = json_decode($qty, true);
            @endphp
        <tr>
            <td>{{ $row->nama_kelurahan }}</td>
            @if (in_array(request()->input('groupby'), [5,6]))
            <td>{{ $row->nama_rw }}</td>
            @endif
            @if (in_array(request()->input('groupby'), [6]))
            <td>{{ $row->nama_rt }}</td>
            @endif
            <td>{{ $qty['3'] }}</td>
            <td>{{ $qty['4'] }}</td>
            <td>{{ $qty['2'] }}</td>
            <td>{{ $qty['5'] }}</td>
            <td>{{ $qty['1'] }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
@endsection