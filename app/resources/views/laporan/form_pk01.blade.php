@extends(request()->ajax() ? 'layouts.ajax' : 'layouts.app')

@section('title', 'PK01')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[1]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[1]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[1]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[2]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[2]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[2]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[3]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[3]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[3]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[4]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[4]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[4]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[5]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[5]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[5]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[6]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[6]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[6]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[7]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[7]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[7]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[8]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[8]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[8]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[9]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[9]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[9]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[10]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[10]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[10]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[11]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[11]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[11]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[12]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[12]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[12]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[13]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[13]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[13]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[14]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[14]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[14]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[15]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[15]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[15]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[16]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[16]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[16]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[17]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[17]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[17]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
        <div class="row kt-padding-10 kt-b-border">
            <div class="col-sm-1">{{ $refpk[18]['id'] }}</div>
            <div class="col-sm-11">
                <p>{{ $refpk[18]['question_text'] }}</p>
                <p><span class="frm-answer" >{{ $frmpk[18]['pilihan_text'] ?? '_' }}</span></p>
            </div>
        </div>
    </div>
</div>



@endsection