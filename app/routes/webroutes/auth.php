<?php
Route::get('login', 'UserController@showLoginForm')->name('login');
Route::post('login', 'UserController@login');
Route::post('logout', 'UserController@logout')->name('logout');;
