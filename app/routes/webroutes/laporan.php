<?php
Route::group(['middleware'=>'auth'], function () {
    Route::group(['prefix'=>'laporan'], function () {
        Route::get('targetactual', 'Laporan\TargetActualController@index');
        Route::get('targetactual/data', 'Laporan\TargetActualController@data');
        Route::get('targetactual/pendata', 'Laporan\TargetActualController@pendata');
        Route::get('statussensus', 'Laporan\StatusSensusController@index');
        Route::get('statussensus/data', 'Laporan\StatusSensusController@data');
        Route::put('statussensus/anulir/{id}', 'Laporan\StatusSensusController@anulir');
        Route::get('getJsData', 'Laporan\Laporan2Controller@getJsData');
        Route::get('cetak', 'Laporan\AnomaliSensusController@cetak');
        Route::get('validme/{id}', 'Laporan\AnomaliSensusController@validme');
        Route::get('anomalisensus', 'Laporan\AnomaliSensusController@index');
        Route::get('anomalisensus/data', 'Laporan\AnomaliSensusController@data');
        Route::get('anomalisensus/indikator/{id}', 'Laporan\AnomaliSensusController@indikator');
        Route::get('anomalisensus/datapaging', 'Laporan\AnomaliSensusController@dataPaging');
        Route::get('summary', 'Laporan\SummaryController@index');
        Route::get('summary/data', 'Laporan\SummaryController@data');
    });
});
