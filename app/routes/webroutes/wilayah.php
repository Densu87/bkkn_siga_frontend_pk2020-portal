<?php
Route::group(['middleware'=>'auth'], function () {
    Route::group(['prefix'=>'wilayah'], function () {
        Route::get('akses', 'WilayahController@akses');
        Route::post('update', 'WilayahController@updateAkses');
        Route::get('provinsi/{id}', 'WilayahController@provinsi');
        Route::get('kotakab/{id}', 'WilayahController@kotakab');
        Route::get('kecamatan/{id}', 'WilayahController@kecamatan');
        Route::get('kelurahan/{id}', 'WilayahController@kelurahan');
        Route::get('rw/{id}', 'WilayahController@rw');
        Route::get('rt/{id}', 'WilayahController@rt');
        Route::get('/', 'WilayahController@index');
        Route::get('getDataProvinsi', 'WilayahController@getDataProvinsi');
        Route::get('getDataKabupaten/{id}', 'WilayahController@getDataKabupaten');
        Route::get('getDataKecamatan/{id}', 'WilayahController@getDataKecamatan');
        Route::get('getDataKelurahan/{id}', 'WilayahController@getDataKelurahan');
        Route::get('getDataRw/{id}', 'WilayahController@getDataRw');
        Route::get('getDataRt/{id}', 'WilayahController@getDataRt');
        Route::get('AddPostRW', 'WilayahController@AddPostRW');
        Route::get('AddPostRT', 'WilayahController@AddPostRT');
        Route::get('EditPostRW', 'WilayahController@EditPostRW');
        Route::get('EditPostRT', 'WilayahController@EditPostRT');
        Route::get('UbahWilayahParent/{id}/{key}/{kondisi}', 'WilayahController@UbahWilayahParent');
    });
});
